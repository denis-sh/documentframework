﻿/**
DocumentFramework project project file.

Copyright: Denis Shelomovskij 2013-2014

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module document.DocumentWindow;


static import std.file;

import unstd.casts;
import unstd.generictuple;

import geometry.in2d.size;

import metaui.controls.all;

import document.utils;
import document.interfaces;
import document.GlobalUI;


@trusted:

abstract class DocumentWindow: Panel!IFileManager
{
	// Fields
	// ----------------------------------------------------------------------------------------------------

	protected
	{
		alias fileManager = source;

		IKeyListener keyListener;

		// File actions
		ActionControl acCreate, acOpen, acSave, acSaveAs, acSaveCopyAs, acRevertToSaved, acClose, acQuit;
		// Edit actions
		ActionControl acUndo, acRedo;
		// Help actions
		ActionControl acAbout;

		ContainerControl ccStart;
		bool haveStartControl = false;
		PanelBase pnlFile = null;

		GlobalUI globalUI;
		package/*FIXME*/ string newFileName;
		FileExtension fileExtension;
		bool isUnsaved;
		bool hasTitle;
		string normalTitle, unsavedTitle;
	}

	private
	{
	}

	// Constructor
	// ----------------------------------------------------------------------------------------------------

	this(GlobalUI globalUI, in bool haveFileManager, string newFileName, FileExtension fileExtension)
	{
		this.globalUI = globalUI;
		this.newFileName = newFileName;
		this.fileExtension = fileExtension;

		acCreate = new ActionControl(ac => createFile());
		acOpen = new ActionControl(ac => openFile());
		acSave = new ActionControl(ac => saveFile());
		acSaveAs = new ActionControl(ac => saveFileAs(false));
		acSaveCopyAs = new ActionControl(ac => saveFileAs(true));
		acRevertToSaved = new ActionControl(ac => revertFileToSaved());
		acClose = new ActionControl((ac) { closeFile(); });
		acQuit = new ActionControl((ac) @trusted { globalUI.tryQuit(); });

		acUndo = new ActionControl((ac) @trusted => fileManager.undo());
		acRedo = new ActionControl((ac) @trusted => fileManager.redo());

		// TODO: Add Window actions

		acAbout = new ActionControl(ac => runAboutDialog());

		ccStart = new ContainerControl();

		super(new ContainerControl(
			acCreate, acOpen, acSave, acSaveAs, acSaveCopyAs, acRevertToSaved, acClose, acQuit,
			acUndo, acRedo,
			acAbout,
			ccStart));

		if(!haveFileManager)
			updateFileOpened();
	}

	// Public functions and properties
	// ----------------------------------------------------------------------------------------------------

	final @property void title(string val)
	{
		hasTitle = true;
		normalTitle = val;
		debug normalTitle ~= " [DEBUG]";
		unsavedTitle = '*' ~ normalTitle;
		updateWindowTitle();
	}

	final @property void documentSaved(in bool val)
	{
		acSave.enabled = !val;
		acRevertToSaved.enabled = !val && fileManager.path;
		isUnsaved = !val;
		if(hasTitle)
			updateWindowTitle();
	}

	final void setCanUndoRedo(in bool canUndo, in bool canRedo)
	{
		acUndo.enabled = canUndo;
		acRedo.enabled = canRedo;
	}





	/*FIXME?*/package final void destroy()
	{
		destroyNative();
	}

	// File actions functions
	// ----------------------------------------------------------------------------------------------------

	final void createFile()
	{
		globalUI.createFile(this);
	}

	final void openFile()
	{
		if(string path = globalUI.getOpenImportPath(this,
			loc("Открыть", "Open"), [fileExtension]))
			globalUI.openFile(this, path);
	}

	final void saveFile()
	in { assert(fileManager); }
	body
	{
		if(!fileManager.path)
		{
			saveFileAs(false);
			return;
		}

		string error;
		if(!fileManager.trySave(error))
			globalUI.showFileSavingError(this, fileManager.path, error);
	}

	final void saveFileAs(in bool asCopy)
	in { assert(fileManager); }
	body
	{
		immutable saved = !!fileManager.path;
		string path = globalUI.getSaveExportPath(this,
			asCopy ? loc("Сохранить копию как", "Save copy as") :
				saved ? loc("Сохранить как", "Save as") : loc("Сохранить", "Save"),
			[fileExtension],
			asCopy ? "Копия %s" : saved ? "%s (2)" : "%s");
		if(!path)
			return;

		string error;
		if(!(asCopy ? fileManager.trySaveCopyAs(path, error) : fileManager.trySaveAs(path, error)))
			globalUI.showFileSavingError(this, path, error);
	}

	final void revertFileToSaved()
	in { assert(fileManager); }
	body
	{
		if(isUnsaved && !globalUI.showQuestion(this,
			loc("Восстановить сохранённыый вариант?", "Restore the saved version?"),
			loc("Все несохранённые изменения будут утеряны", "All unsaved changes will be lost"),
			false
		))
			return;
		string error;
		if(!fileManager.tryRevertToSaved(error))
			globalUI.showFileOpeningError(this, fileManager.path, error);
	}

	final bool closeFile()
	in { assert(fileManager); }
	body
	{
		if(!tryCloseFile())
			return false;
		globalUI.onFileClosed(this);
		return true;
	}

	final bool tryCloseFile()
	in { assert(fileManager); }
	body
	{
		if(!askCloseFile())
			return false;
		fileManager.close();
		return true;
	}

	private bool askCloseFile()
	in { assert(fileManager); }
	body
	{
		if(!isUnsaved)
			return true;

		immutable res = globalUI.showQuestion3(this,
			fileManager.path ?
				loc("Сохранить изменения в чертеже\n'%s'\nперед закрытием?", "Save changes in the draft\n'%s'\nbefore closing?", fileManager.path) :
				loc("Сохранить изменения в чертеже\nперед закрытием?", "Save changes in the draft\nbefore closing?"),
			loc("Все несохранённые изменения будут утеряны", "All unsaved changes will be lost"),
			Question3Res.yes
		);
		final switch(res)
		{
			case Question3Res.yes:
				saveFile();
				return !isUnsaved;
			case Question3Res.no:
				return true;
			case Question3Res.cancel:
				return false;
		}
	}

protected:

	// Manager setting functions
	// ----------------------------------------------------------------------------------------------------

	override void connectSource()
	{ updateFileOpened(); keyListener = fileManager.tryDynamicCast!IKeyListener; }

	override void disconnectSource()
	{ updateFileOpened(); keyListener = null; }

	// Abstract functions
	// ----------------------------------------------------------------------------------------------------

	abstract
	{
		void hidePnlFileWrapper();
		void showPnlFileWrapper();
		void addStartControl();
		PanelBase createFilePanel();
		void addFileControl();
		void setWindowTitle(string title);
		void runAboutDialog();
		void destroyNative();
	}

	// Callbacks for native implementation
	// ----------------------------------------------------------------------------------------------------

	final bool onWindowClosing()
	{
		if(fileManager && !tryCloseFile())
			return true;
		globalUI.onWindowClosed(this);
		return false;
	}

	final bool onKeyPress(in Key key, in ModifierKey modifier)
	{
		if(!keyListener)
			return false;
		return keyListener.onKeyPress(key, modifier);
	}

	final bool onKeyRelease(in Key key, in ModifierKey modifier)
	{
		if(!keyListener)
			return false;
		return keyListener.onKeyRelease(key, modifier);
	}

private:

	// Update functions
	// ----------------------------------------------------------------------------------------------------

	void updateWindowTitle()
	{ setWindowTitle(isUnsaved ? unsavedTitle : normalTitle); }

	void updateFileOpened()
	{
		const fileOpened = !!fileManager;
		foreach(ac; GenericTuple!(acSaveAs, acSaveCopyAs, acClose))
			ac.enabled = fileOpened;

		if(!fileOpened)
		{
			if(pnlFile)
			{
				pnlFile.rootControl.hide();
				hidePnlFileWrapper();
			}
			showStartControl();

			isUnsaved = false;
			title = "Начальная страница";
			foreach(ac; GenericTuple!(acSave, acRevertToSaved, acUndo, acRedo))
				ac.enabled = false;
		}
		else
		{
			if(haveStartControl)
				ccStart.hide();
			showFilePanel();
			showPnlFileWrapper();
		}
	}

	// Controls creation functions
	// ----------------------------------------------------------------------------------------------------

	void showStartControl()
	{
		if(!haveStartControl)
		{
			addStartControl();
			haveStartControl = true;
		}
		ccStart.show();
	}

	void showFilePanel()
	{
		if(!pnlFile)
		{
			pnlFile = createFilePanel();
			addFileControl();
		}
		pnlFile.rootControl.show();
	}
}
