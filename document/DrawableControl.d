﻿/**
DocumentFramework project project file.

Copyright: Denis Shelomovskij 2013

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module document.DrawableControl;


import std.algorithm;

import geometry.in2d.size;
import geometry.in2d.point;

import metaui.controls.all;

import document.utils;
import document.interfaces;


@safe:

abstract class DrawableControl: Panel!IDrawable
{
	// Fields
	// ----------------------------------------------------------------------------------------------------

	private
	{
		alias drawable = source;

		bool hasSize = false, hasMouse = false, mouseIn = false;
		SizeI size;
		Point mouse;
		ModifierKey modifier;
		ubyte[MouseButton.max + 1] pressedButtons;
	}

	// Constructor
	// ----------------------------------------------------------------------------------------------------

	this()
	{
		super(new ContainerControl());
	}

	// Abstract public functions
	// ----------------------------------------------------------------------------------------------------

	abstract void queueRedraw();

protected:

	// Manager setting functions
	// ----------------------------------------------------------------------------------------------------

	override void connectSource() @trusted
	{
		pressedButtons[] = 0;
		if(hasSize)
		{
			drawable.onResize(size);
			if(mouseIn)
				drawable.onMouseAction(mouse, modifier, MouseAction.enter);
		}
	}

	override void disconnectSource()
	{ }

	// Callbacks for native implementation
	// ----------------------------------------------------------------------------------------------------

	final void onResized(in SizeI size)
	{
		if(hasSize && this.size == size)
			return; // Ignoring repeated resize.
		hasSize = true;
		this.size = size;
		if(drawable)
			drawable.onResize(size);
	}

	final void onMouseAction(in Point mouse, in ModifierKey modifier, in MouseAction action)
	{
		if(!hasSize)
			return; // Ignoring action if there is no size yet.

		MouseAction effectiveAction = action;
		final switch(action)
		{
			case MouseAction.leave:
				if(!mouseIn)
					return; // Ignoring repeated leave of first action as leave.
				break;
			case MouseAction.enter:
				if(mouseIn)
					effectiveAction = MouseAction.motion; // Treating repeated enter as motion.
				break;
			case MouseAction.motion:
				if(!mouseIn)
					effectiveAction = MouseAction.enter; // Treating motion as missing enter.
				break;
		}

		if(effectiveAction == MouseAction.motion && hasMouse && this.mouse == mouse && this.modifier == modifier)
			return; // Ignoring repeated motion.

		hasMouse = true;
		this.mouse = mouse;
		this.modifier = modifier;
		if(drawable && hasSize)
			drawable.onMouseAction(mouse, modifier, effectiveAction);
	}

	final void onMouseDown(in ubyte count, in MouseButton button, in ModifierKey modifier)
	{
		if(!hasSize)
			return; // Ignoring action if there is no size yet.

		const sequential = count > 1;

		if(pressedButtons[button] >= 1 + sequential)
			return; // Ignoring repeated [sequential] mouse down events.

		pressedButtons[button] = 1 + sequential;

		if(drawable)
		{
			if(!sequential)
				drawable.onMouseDown(button, modifier);
			else
				drawable.onMouseSequentialDown(count, button, modifier);
		}
	}

	final void onMouseUp(in MouseButton button, in ModifierKey modifier)
	{
		if(!hasSize)
			return; // Ignoring action if there is no size yet.

		if(!pressedButtons[button].tryUpdate(cast(ubyte) 0))
			return; // Ignoring repeated mouse up events.

		if(drawable)
			drawable.onMouseUp(button, modifier);
	}

	final void onMouseScroll(in ScrollDirection direction, in ModifierKey modifier)
	{
		if(!hasSize)
			return; // Ignoring action if there is no size yet.

		if(drawable)
			drawable.onMouseScroll(direction, modifier);
	}

	void onDraw()
	{
		if(drawable)
			drawable.onDraw();
	}
}
