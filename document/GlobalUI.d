﻿/**
DocumentFramework project project file.

Copyright: Denis Shelomovskij 2013

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module document.GlobalUI;


import unstd.casts;

import std.array; // for empty
import std.exception;
import std.path;
import std.string;

import metaui.controls.native;
import metaui.controls.panel;

import document.utils;

import document.interfaces;
import document.DocumentWindow;


@safe:

struct FileExtension
{ string title, ext; }

enum Question3Res
{
	yes = 0,
	no,
	cancel
}

interface WindowFactory // FIXME DocumentFactory ?
{
	DocumentWindow create(GlobalUI globalUI, IFileManager fileManager);
}

abstract class GlobalUI: IGlobalUI
{
	// Fields
	// ----------------------------------------------------------------------------------------------------

	private
	{
		bool noFilesOpened = true;
		WindowFactory windowFactory;
		IFileManagerFactory fileMgrFactory;
		OrderedObjectSet!DocumentWindow windows;
	}

	invariant()
	{
		assert(noFilesOpened == windows.empty || (windows.length == 1) && !windows.buff[0].source);
	}

	// Constructor
	// ----------------------------------------------------------------------------------------------------

	this(WindowFactory windowFactory)
	in { assert(windowFactory); }
	body
	{
		this.windowFactory = windowFactory;
	}

	// Public functions and properties
	// ----------------------------------------------------------------------------------------------------

	final void initialize()
	{
		initializeNative();
	}

	final void run(IFileManagerFactory fileMgrFactory, const string[] paths)
	{
		this.fileMgrFactory = fileMgrFactory;

		DocumentWindow lastWindow = null;
		foreach(const path; paths)
		{
			string error;
			auto fileManager = fileMgrFactory.tryOpenFile(path, error);
			if(!fileManager)
			{
				if(!lastWindow)
					windows ~= (lastWindow = windowFactory.create(this, null));
				showFileOpeningError(lastWindow, path, error);
				continue;
			}

			noFilesOpened = false;
			if(lastWindow && !lastWindow.source)
			{
				lastWindow.source = fileManager;
			}
			else
			{
				windows ~= (lastWindow = windowFactory.create(this, fileManager));
			}
		}

		if(paths.empty)
			windows ~= windowFactory.create(this, null);

		runNative();
	}

	// IO functions
	// ----------------------------------------------------------------------------------------------------

	final bool tryReadData(PanelBase panel, string path, out void[] data)
	{
		if(const msg = collectExceptionMsg!(std.file.FileException)(data = std.file.read(path)))
		{
			showFileReadingError(panel, path, msg);
			return false;
		}
		return true;
	}

	final bool tryWriteData(PanelBase panel, string path, in void[] data)
	{
		if(const msg = collectExceptionMsg!(std.file.FileException)(std.file.write(path, data)))
		{
			showFileWritingError(panel, path, msg);
			return false;
		}
		return true;
	}

	// Dialog functions
	// ----------------------------------------------------------------------------------------------------

	private DocumentWindow getDocumentWindow(PanelBase panel)
	{
		for(;;)
		{
			if(auto doc = panel.tryDowncast!DocumentWindow)
				return doc;
			panel = panel.parent;
		}
	}

	final string getOpenImportPath(PanelBase panel,
		in string title,
		in FileExtension[] extensions)
	{
		string path;
		const doc = getDocumentWindow(panel);
		const savedPath = doc.fileManager ? doc.fileManager.path : null;
		if(!runOpenSaveDialog(panel.rootControl.native,
			true, title, extensions,
			null,
			savedPath ? savedPath.dirName() : null,
			null,
			path))
			return null;
		return path;
	}

	final string getSaveExportPath(PanelBase panel,
		in string title,
		in FileExtension[] extensions,
		in string fileNameFormat)
	{
		string path;
		const doc = getDocumentWindow(panel);
		const savedPath = doc.fileManager ? doc.fileManager.path : null;
		if(!runOpenSaveDialog(panel.rootControl.native,
			false, title, extensions,
			null,
			savedPath ? savedPath.dirName() : null,
			format(fileNameFormat, savedPath ? savedPath.baseName().stripExtension() : doc.newFileName),
			path))
			return null;
		return path;
	}

	// Messages functions
	// ----------------------------------------------------------------------------------------------------

	final void showApplicationLaunchingError(string text, string secondaryText)
	{ runErrorDialog(null, text, secondaryText); }

	final void showError(PanelBase panel, string text, string secondaryText)
	{ runErrorDialog(panel.rootControl.native, text, secondaryText); }

	final void showFileOpeningError(PanelBase panel, string path, string error)
	{ showErrorWithSuberror(panel, loc("Ошибка при открытии файла\n'%s'", "Error opening file\n'%s'", path), error); }

	final void showInformation(PanelBase panel, string text)
	{ runInformationDialog(panel.rootControl.native, text); }

	final bool showQuestion(PanelBase panel, string text, string secondaryText, bool defaultResponse)
	{ return runQuestionDialog(panel.rootControl.native, text, secondaryText, defaultResponse); }

	final Question3Res showQuestion3(PanelBase panel, string text, string secondaryText, Question3Res defaultResponse)
	{ return runQuestion3Dialog(panel.rootControl.native, text, secondaryText, defaultResponse); }

	// Error messages functions
	// ----------------------------------------------------------------------------------------------------

	void showErrorWithSuberror(PanelBase panel, string text, string suberrorText)
	{ runErrorDialog(panel.rootControl.native, text, loc("Текст ошибки:\n%s", "Error text:\n%s", suberrorText)); }

	void showFileReadingError(PanelBase panel, string path, string error)
	{ showErrorWithSuberror(panel, loc("Ошибка при чтении файла\n'%s'", "Error reading file\n'%s'", path), error); }

	void showFileSavingError(PanelBase panel, string path, string error)
	{ showErrorWithSuberror(panel, loc("Ошибка при сохранении файла\n'%s'", "Error saving file\n'%s'", path), error); }

	void showFileWritingError(PanelBase panel, string path, string error)
	{ showErrorWithSuberror(panel, loc("Ошибка при записи файла\n'%s'", "Error writing file\n'%s'", path), error); }
	

protected:

	// Abstract functions
	// ----------------------------------------------------------------------------------------------------

	abstract
	{
		void initializeNative();
		void runNative();
		void quitNative();

		// Dialogs run functions
		bool runOpenSaveDialog(INativeControl control, 
			in bool openDialog, string title,
			in FileExtension[] extensions,
			in string currentPath, in string currentFolder, in string currentName,
			out string path);

		void runInformationDialog(INativeControl control, string text);
		bool runQuestionDialog(INativeControl control, string text, string secondaryText, bool defaultResponse);
		Question3Res runQuestion3Dialog(INativeControl control, string text, string secondaryText, Question3Res defaultResponse);
		void runErrorDialog(INativeControl control, string text, string secondaryText);
	}

package:

	void createFile(DocumentWindow sender)
	out { assert(!noFilesOpened); }
	body
	{
		auto fileManager = fileMgrFactory.createFile();
		if(!sender.source)
		{
			sender.source = fileManager;
			noFilesOpened = false;
		}
		else
		{
			windows ~= windowFactory.create(this, fileManager);
		}
	}

	void openFile(DocumentWindow sender, string path)
	in { assert(path); }
	out { assert(!noFilesOpened); }
	body
	{
		string error;
		auto fileManager = fileMgrFactory.tryOpenFile(path, error);
		if(!fileManager)
		{
			showFileOpeningError(sender, path, error);
			return;
		}
		if(!sender.source)
		{
			sender.source = fileManager;
			noFilesOpened = false;
		}
		else
		{
			windows ~= windowFactory.create(this, fileManager);
		}
	}

	void onFileClosed(DocumentWindow window)
	in { assert(windows.contains(window)); }
	body
	{
		if(windows.length > 1)
		{
			windows.remove(window);
			window.destroy();
		}
		else
		{
			window.source = null;
			noFilesOpened = true;
		}
	}

	void onWindowClosed(DocumentWindow window)
	{
		windows.remove(window);
		/*immutable index = windows.countUntil(window);
		windows = windows.remove(index);
		foreach(w; windows)
			w.onOtherWindowClosed(index);*/
		if(windows.empty)
		{
			noFilesOpened = true;
			quitNative();
		}
	}

	bool tryQuit()
	{
		auto windowsBuff = windows.releaseBuff();
		if(noFilesOpened)
		{
			windowsBuff[0].destroy();
		}
		else
		{
			noFilesOpened = true;
			foreach(i, window; windowsBuff)
				if(!window.tryCloseFile())
				{
					foreach(windowToDestroy; windowsBuff[0 .. i])
						windowToDestroy.destroy();
					windows.copyFrom(windowsBuff[i .. $]);
					noFilesOpened = false;
					return false;
				}
		}

		quitNative();
		return true;
	}
}
