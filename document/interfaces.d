﻿/**
DocumentFramework project project file.

Copyright: Denis Shelomovskij 2013

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module document.interfaces;


import geometry.in2d.size;
import geometry.in2d.point;


@safe:

// Global UI
// ----------------------------------------------------------------------------------------------------

interface IGlobalUI
{
	void initialize();
	void run(IFileManagerFactory fileMgrFactory, const string[] paths);
}

// Document
// ----------------------------------------------------------------------------------------------------

interface IFileManager
{
	bool trySave(out string error)
	in { assert(path); }

	bool trySaveAs(string path, out string error)
	out { assert(path); }

	bool trySaveCopyAs(string path, out string error);

	bool tryRevertToSaved(out string error)
	in { assert(path); }

	void close();

	@property string path() const; // null if is not saved

	void undo();
	void redo();
}

interface IFileManagerFactory
{
	IFileManager createFile();
	IFileManager tryOpenFile(string path, out string error);
}

final class SimpleFileManagerFactory(T, E): IFileManagerFactory
{
	override IFileManager createFile()
	{ return new T(); }

	override IFileManager tryOpenFile(string path, out string error)
	{
		try return new T(path);
		catch(E e) error = e.msg;
		return null;
	}
}

// Key listener
// ----------------------------------------------------------------------------------------------------

enum ModifierKey { none = 0, shift = 1, control = 2, alt = 4 }

enum Key
{
	modifier,
	del,
	other,
}

interface IKeyListener
{
	bool onKeyPress(in Key key, in ModifierKey modifier);
	bool onKeyRelease(in Key key, in ModifierKey modifier);
}

// Drawable
// ----------------------------------------------------------------------------------------------------

enum MouseButton
{
	left, middle, right,
	x1, x2,
	other,
}

enum ScrollDirection { up, down, left, right }

enum MouseAction
{
	enter,
	leave,
	motion,
}


interface IDrawable
{
	void onResize(in SizeI wndSize);

	void onMouseAction(in Point mouse, in ModifierKey modifier, in MouseAction action);
	void onMouseDown(in MouseButton button, in ModifierKey modifier);
	void onMouseSequentialDown(in ubyte count, in MouseButton button, in ModifierKey modifier);
	void onMouseUp(in MouseButton button, in ModifierKey modifier);
	void onMouseScroll(in ScrollDirection direction, in ModifierKey modifier);

	void onDraw();
}