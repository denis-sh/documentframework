﻿/**
DocumentFramework project project file.

Copyright: Denis Shelomovskij 2013-2014

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module document.native.GTKDocumentWindow;


import unstd.casts;

import drawing.native.CairoHelper;

import gdk.Keysyms;
import gdk.Event;

import cairo.Context;

import gtk.Widget;
import gtk.Window;
import gtk.Dialog;
import gtk.VBox;

import gtk.MenuBar;
import gtk.Menu;
import gtk.AccelGroup;

static import gtkd.gtk.controls;
import gtkd.gtkdcontrols;

import document.utils;
import document.interfaces;
import document.GlobalUI;
import document.DocumentWindow;
import document.native.GTKGlobalUI;


@safe:

abstract class GTKDocumentWindow: DocumentWindow
{
	// Fields
	// ----------------------------------------------------------------------------------------------------

	private
	{
		public /*protected*/ /*FIXME*/ Window window;
		VBox vboxMain;
		Widget save, saveAs, saveCopyAs, revertToSaved, close, undo, redo;
		//RadioMenuItem[] windows;

		bool hasHeaderWidget = false;
	}

	// Constructor
	// ----------------------------------------------------------------------------------------------------

	this(GTKGlobalUI globalUI, IFileManager fileManager, string newFileName, FileExtension fileExtension, StockID icon) @trusted
	{
		window = new Window(GtkWindowType.TOPLEVEL);
		// FIXME delete? if(icon != -1)
		// FIXME delete? 	window.setIconNameFixed(StockDesc[icon]);

		vboxMain = new VBox(false, 0);

		vboxMain.show();

		window.add(vboxMain);
		window.resize(900, 600);
		window.addOnDelete(&onWindowDelete);

		// Fixed in GtkD f89ffebea3f0e49eb4faeef4f7ff2fbc3ea3d1b3
		// TODO why window captures in without `gtk_widget_add_events`?
		//window.addEvents(EventMask.KEY_PRESS_MASK); // FIXME Why GtkD doesn't do it?
		window.addOnKeyPress(&onKeyPress);
		//window.addEvents(EventMask.KEY_RELEASE_MASK); // FIXME Why GtkD doesn't do it?
		window.addOnKeyRelease(&onKeyRelease);
		//window.grabFocus();

		super(globalUI, !!fileManager, newFileName, fileExtension);
		createMenu();
		source = fileManager;
		rootControl.native = new gtkd.gtk.controls.Window(window.getWindowStruct());
	}

	// Public functions
	// ----------------------------------------------------------------------------------------------------

	final void setTransientForThis(Dialog dialog) @trusted
	{
		dialog.setTransientFor(window);
	}

protected:

	// Abstract function implementation
	// ----------------------------------------------------------------------------------------------------

	Widget pnlFileWrapper;

	final override @trusted
	{

		void hidePnlFileWrapper()
		{ pnlFileWrapper.hide(); }

		void showPnlFileWrapper()
		{ pnlFileWrapper.show(); }

		void addStartControl()
		{
			import gtkc.gtk;

			auto startWidget = toGTKDWidget(createStartWidget());
			ccStart.native = startWidget;
			vboxMain.getBoxStruct().gtk_box_pack_end(startWidget.gtkWidget, true, true, 0);
			//vboxMain.packStart(startWidget, true, true, 0);
		}

		void addFileControl()
		{
			vboxMain.packStart(pnlFileWrapper = toGtkDWidget(pnlFile.rootControl.native.dynamicCast!(gtkd.gtk.controls.Widget)), true, true, 0);
		}

		void setWindowTitle(string title)
		{ window.setTitle(title); }

		void runAboutDialog()
		{
			import gtk.AboutDialog;

			auto d = createAboutDialog();
			d.setTransientFor(window);
			d.run();
			d.destroy();
		}

		void destroyNative() @trusted
		{
			window.destroy();
		}
	}

	// Abstract widgets creation functions
	// ----------------------------------------------------------------------------------------------------

	abstract void createMenuWidgets(MenuBar menuBar, Menu file, Menu edit, Menu help, AccelGroup ag);
	abstract Dialog createAboutDialog();

	// Virtual widgets creation functions
	// ----------------------------------------------------------------------------------------------------

	Widget createHeaderWidget()
	{ return null; }

	Widget createStartWidget() @trusted
	{
		import gtk.Button;
		import gtk.VButtonBox;

		auto buttonBox = VButtonBox.createActionBox();
		with(buttonBox)
		{
			packStart(new Button(StockID.NEW, (button) { acCreate.activate(); }), false, false, 0);
			packStart(new Button(StockID.OPEN, (button) { acOpen.activate(); }), false, false, 0);
		}

		Widget startWidget = buttonBox;
		if(auto header = createHeaderWidget())
		{
			auto vbox = new VBox(false, 10);
			with(vbox)
			{
				packStart(header, false, false, 0);
				packStart(buttonBox, false, false, 0);
			}
			startWidget = vbox;
			hasHeaderWidget = true;
		}

		import gtk.Alignment;
		auto alignment = Alignment.center(startWidget);
		alignment.showAll();
		alignment.addOnDraw(&onStartButtonBoxDraw);
		return alignment;
	}

private:

	// Signal handlers
	// ----------------------------------------------------------------------------------------------------

	bool onWindowDelete(Event, Widget)
	{ return onWindowClosing(); }

	/*void onOtherWindowOpened(string title)
	{
		addOnFocusIn((event, widget) { return false; });
		addOnFocusOut((event, widget) { return false; });

		new RadioMenuItem(cast(ListSG) null, title);
		windows ~= [].destroy();
		windows = windows.remove(index);
	}

	void onOtherWindowClosed(size_t index)
	{
		windows[index].destroy();
		windows = windows.remove(index);
	}*/

	bool onKeyPress(Event e, Widget) @trusted
	in { assert(e.type() == GdkEventType.KEY_PRESS); }
	body
	{
		const key = e.key();
		auto mod = gdkStateToModifierKey(key.state);
		with(GdkKeysyms) if(key.keyval == GDK_Alt_L || key.keyval == GDK_Alt_R)
			mod |= ModifierKey.alt; // As there is no state mask on Alt key press event.
		return super.onKeyPress(gdkEventKeyToKey(key), mod);
	}

	bool onKeyRelease(Event e, Widget) @trusted
	in { assert(e.type() == GdkEventType.KEY_RELEASE); }
	body
	{
		const key = e.key();
		return super.onKeyRelease(gdkEventKeyToKey(key), gdkStateToModifierKey(key.state));
	}

	bool onStartButtonBoxDraw(Context context, Widget sender) @trusted
	{
		// Workaround GtkD @@@BUG35@@@
		static assert(is(typeof(context.tupleof[0]) == cairo_t*));
		scope(exit) context.tupleof[0] = null;

		cairo_rectangle_int_t srect, rect;
		sender.getAllocation(srect);

		import gtk.Bin;
		(cast(Bin) sender).getChild().getAllocation(rect);

		alias context cr;
		cr.translate(0, 0);

		enum startWidgetPadding = 15, startWidgetShadow = 10;
		cr.setSourceRgba(1.0, 1.0, 1.0, 1.0);

		if(!hasHeaderWidget)
		{
			rect.width += 2 * startWidgetPadding;
			rect.height += 2 * startWidgetPadding;
		}
		rect.x = (srect.width - rect.width) / 2;
		rect.y = (srect.height - rect.height) / 2;

		cr.rectangle(rect.tupleof);
		cr.fill();
		import drawing.color;
		cr.fillBorderShadow(Color(0, 0, 0, 102), rect.tupleof, startWidgetShadow);
		return false;
	}

	// Other functions
	// ----------------------------------------------------------------------------------------------------

	/+
	final Window createPanelWindow(string title, SizeI initialSize, Widget rootWidget)
	{
		auto wnd = new Window(title);
		with(wnd)
		{
			setDestroyWithParent(true);
			setSkipTaskbarHint(true);
			setTypeHint(GdkWindowTypeHint.DIALOG);
			setBorderWidth(5);
			setTransientFor(window);
			resize(initialSize.tupleof);
			addOnDelete((e, w) => (w.hide(), true));
			add(rootWidget);
		}
		return wnd;
	}
	+/



	void createMenu() @trusted
	{
		import gtk.MenuItem;
		import gtk.ImageMenuItem;
		import gtk.RadioMenuItem;
		import gtk.CheckMenuItem;
		import gtk.SeparatorMenuItem;

		auto ag = new AccelGroup();

		auto menuBar = new MenuBar();
		with(menuBar)
		{
			MenuItem tmp;

			Menu fileMenu, editMenu, windowMenu, helpMenu;
			with(fileMenu = append(loc("_Файл", "_File")))
			{
				append(tmp = new ImageMenuItem(StockID.NEW, ag));
				tmp.setLabel(loc("_Создать", "_Create"));
				acCreate.native = new NativeGTKDMenuItem(tmp);

				append(tmp = new ImageMenuItem(StockID.OPEN, ag));
				acOpen.native = new NativeGTKDMenuItem(tmp);
				tmp.addAccelerator("activate", ag, GdkKeysyms.GDK_F5, ModifierType.MOD1_MASK, AccelFlags.VISIBLE); // For Windows 6.x

				append(save = tmp = new ImageMenuItem(StockID.SAVE, ag));
				acSave.native = new NativeGTKDMenuItem(tmp);
				tmp.addAccelerator("activate", ag, GdkKeysyms.GDK_F6, ModifierType.MOD1_MASK, AccelFlags.VISIBLE); // For Windows 6.x

				append(saveAs = tmp = new ImageMenuItem(StockID.SAVE_AS, null));
				tmp.addAccelerator("activate", ag, GdkKeysyms.GDK_s, ModifierType.SHIFT_MASK | ModifierType.CONTROL_MASK, AccelFlags.VISIBLE);
				acSaveAs.native = new NativeGTKDMenuItem(tmp);
				tmp.addAccelerator("activate", ag, GdkKeysyms.GDK_F7, ModifierType.MOD1_MASK, AccelFlags.VISIBLE); // For Windows 6.x

				append(saveCopyAs = tmp = new ImageMenuItem(StockID.SAVE_AS, null));
				tmp.setLabel(loc("Сохранить _копию как...", "Save _copy as..."));
				acSaveCopyAs.native = new NativeGTKDMenuItem(tmp);

				append(revertToSaved = tmp = new ImageMenuItem(StockID.REVERT_TO_SAVED, null));
				tmp.addAccelerator("activate", ag, GdkKeysyms.GDK_r, ModifierType.CONTROL_MASK, AccelFlags.VISIBLE);
				acRevertToSaved.native = new NativeGTKDMenuItem(tmp);
				tmp.addAccelerator("activate", ag, GdkKeysyms.GDK_F8, ModifierType.MOD1_MASK, AccelFlags.VISIBLE); // For Windows 6.x

				append(close = tmp = new ImageMenuItem(StockID.CLOSE, ag));
				acClose.native = new NativeGTKDMenuItem(tmp);
				tmp.addAccelerator("activate", ag, GdkKeysyms.GDK_F9, ModifierType.MOD1_MASK, AccelFlags.VISIBLE); // For Windows 6.x
			}

			with(editMenu = append(loc("_Правка", "_Edit")))
			{
				append(undo = tmp = new ImageMenuItem(StockID.UNDO, null));
				tmp.addAccelerator("activate", ag, GdkKeysyms.GDK_z, ModifierType.CONTROL_MASK, AccelFlags.VISIBLE);
				acUndo.native = new NativeGTKDMenuItem(tmp);
				tmp.addAccelerator("activate", ag, GdkKeysyms.GDK_z, ModifierType.MOD1_MASK, AccelFlags.VISIBLE); // For Windows 6.x
				tmp.addAccelerator("activate", ag, GdkKeysyms.GDK_F2, ModifierType.MOD1_MASK, AccelFlags.VISIBLE); // For Windows 6.x

				append(redo = tmp = new ImageMenuItem(StockID.REDO, null));
				tmp.addAccelerator("activate", ag, GdkKeysyms.GDK_y, ModifierType.CONTROL_MASK, AccelFlags.VISIBLE);
				tmp.addAccelerator("activate", ag, GdkKeysyms.GDK_z, ModifierType.SHIFT_MASK | ModifierType.CONTROL_MASK, AccelFlags.VISIBLE);
				acRedo.native = new NativeGTKDMenuItem(tmp);
				tmp.addAccelerator("activate", ag, GdkKeysyms.GDK_y, ModifierType.MOD1_MASK, AccelFlags.VISIBLE); // For Windows 6.x
				tmp.addAccelerator("activate", ag, GdkKeysyms.GDK_F3, ModifierType.MOD1_MASK, AccelFlags.VISIBLE); // For Windows 6.x
			}

			/*auto windowMenuItem = new MenuItem(loc("_Окно", "_Window"));
			windowMenu = new Menu();
			windowMenuItem.setSubmenu(windowMenu);*/

			auto helpMenuItem = new MenuItem(loc("_Справка", "_Help"));
			helpMenu = new Menu();
			helpMenuItem.setSubmenu(helpMenu);
			with(helpMenu)
			{
				append(tmp = new ImageMenuItem(StockID.ABOUT, ag));
				tmp.addAccelerator("activate", ag, GdkKeysyms.GDK_F1, ModifierType.MOD1_MASK, AccelFlags.VISIBLE);
				acAbout.native = new NativeGTKDMenuItem(tmp);
			}

			createMenuWidgets(menuBar, fileMenu, editMenu, helpMenu, ag);

			with(fileMenu)
			{
				append(new SeparatorMenuItem());

				append(tmp = new ImageMenuItem(StockID.QUIT, ag));
				acQuit.native = new NativeGTKDMenuItem(tmp);
			}

			//menuBar.append(windowMenuItem);
			menuBar.append(helpMenuItem);
		}

		window.addAccelGroup(ag);
		menuBar.showAll();
		vboxMain.packStart(menuBar, false, true, 0);
	}


}

package:

ModifierKey gdkStateToModifierKey(uint state, uint keyval = 0)
{
	auto mod = ModifierKey.none;
	if(state & GdkModifierType.SHIFT_MASK)
		mod |= ModifierKey.shift;
	if(state & GdkModifierType.CONTROL_MASK)
		mod |= ModifierKey.control;
	if(state & GdkModifierType.MOD1_MASK)
		mod |= ModifierKey.alt;
	return mod;
}

private:

Key gdkEventKeyToKey(in GdkEventKey* gdkKey)
{
	with(Key) with(GdkKeysyms) switch(gdkKey.keyval)
	{
		case GDK_Shift_L: case GDK_Shift_R:
		case GDK_Control_L: case GDK_Control_R:
		case GDK_Alt_L: case GDK_Alt_R:
			return modifier;
		case GDK_Delete:
			return del;
		default:
			if(gdkKey.hardwareKeycode >= 'A' && gdkKey.hardwareKeycode <= 'Z')
				return cast(Key) gdkKey.hardwareKeycode;
			return other;
	}
}

__EOF__
/// GTK+ `gtk_window_set_icon_name` @@@BUG@@@ workaround

private string[string] _setIconNameFixedFiles;

void addIconForSetIconNameFixed(string name, string filePath)
{
	_setIconNameFixedFiles[name] = filePath;
}

private void setIconNameFixed(Window window, string name)
{
	if(const filePath = name in _setIconNameFixedFiles)
		window.setIconFromFile(*filePath);
	else
		window.setIconName(name);
}
