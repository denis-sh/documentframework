﻿/**
DocumentFramework project project file.

Copyright: Denis Shelomovskij 2013

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module document.native.GTKDrawableControl;


import geometry.in2d.size;
import geometry.in2d.point;

import gdk.Event;

import cairo.Context;

import gtk.Widget;
import gtk.DrawingArea;

import gtkd.gtkdcontrols;

import document.interfaces;
import document.DrawableControl;
import document.native.GTKDocumentWindow: gdkStateToModifierKey;


@safe:

interface ICairoDrawer
{
	void setContext(Context context);
}

final class GTKDrawableControl: DrawableControl
{
	// Fields
	// ----------------------------------------------------------------------------------------------------

	private
	{
		ICairoDrawer cairoDrawer;
		DrawingArea drawingArea;
	}

	// Constructor
	// ----------------------------------------------------------------------------------------------------

	this() @trusted
	{
		with(drawingArea = new DrawingArea())
		{
			addOnDraw(&onDraw);

			addOnMotionNotify(&onMotionNotify);
			addOnButtonPress(&onButtonPress);
			addOnButtonRelease(&onButtonRelease);
			addEvents(EventMask.SCROLL_MASK); // Don't add SMOOTH_SCROLL_MASK
			addOnScroll(&onScroll);

			//assert(!getCanFocus());
			//setCanFocus(true);
			//assert(getCanFocus()); FIXME why not work?

			addEvents(EventMask.ALL_EVENTS_MASK);
			addOnEnterNotify(&onEnterLeaveNotify);
			addOnLeaveNotify(&onEnterLeaveNotify);

			addOnSizeAllocate(&onSizeAllocate);
		}

		super();

		drawingArea.show(); // As NativeGTKDWidget
		rootControl.native = new NativeGTKDWidget(drawingArea);
	}

	// Public functions and properties
	// ----------------------------------------------------------------------------------------------------

	final @property inout(Widget) widget() inout
	{ return drawingArea; }

	void setCairoDrawer(ICairoDrawer cairoDrawer)
	{
		this.cairoDrawer = cairoDrawer;
	}

	// Abstract public functions implementation
	// ----------------------------------------------------------------------------------------------------

	override void queueRedraw() @trusted
	{
		drawingArea.queueDraw();
	}

private:

	// Signal handlers
	// ----------------------------------------------------------------------------------------------------

	void onSizeAllocate(GtkAllocation* a, Widget)
	{
		onResized(SizeI(a.width, a.height));
		//drawingArea.waitGdk(); FIXME ???
	}

	bool onEnterLeaveNotify(Event e, Widget) @trusted
	in { assert(e.type() == GdkEventType.ENTER_NOTIFY || e.type() == GdkEventType.LEAVE_NOTIFY); }
	body
	{//log(e.type());
		// Double LEAVE_NOTIFY possible when dragging out of widget
		const action = e.type() == GdkEventType.ENTER_NOTIFY ? MouseAction.enter : MouseAction.leave;
		m_onMouseAction(e, action);
		return false;//FIXME why false?
	}

	bool onMotionNotify(Event e, Widget) @trusted
	in { assert(e.type() == GdkEventType.MOTION_NOTIFY); }
	body
	{
		// MOTION_NOTIFY can be sent after LEAVE_NOTIFY when dragging out of widget
		m_onMouseAction(e, MouseAction.motion);
		return true;
	}

	// Required as local mouse position can change e.g.
	// because of window location/size change.
	void m_onMouseAction(Event e, MouseAction mouseAction) @trusted
	{
		Point mouse;
		GdkModifierType mouseModifierType;
		if(e)
		{
			double xWin, yWin;
			int res = e.getCoords(xWin, yWin);
			assert(res);
			mouse = Point(xWin, yWin);
			res = e.getState(mouseModifierType);
			assert(res);
		}
		else
		{
			// FIXME needed?
			mouse = drawingArea.getClientPointer(mouseModifierType);
		}

		onMouseAction(mouse, gdkStateToModifierKey(mouseModifierType), mouseAction);
	}

	bool onButtonPress(Event e, Widget) @trusted
	in { assert(e.type() == GdkEventType.BUTTON_PRESS || e.type() == GdkEventType.DOUBLE_BUTTON_PRESS || e.type() == GdkEventType.TRIPLE_BUTTON_PRESS); }
	body
	{//log("onButtonPress");
		m_onMouseAction(e, MouseAction.motion);//FIXME

		const btn = e.button();
		const ubyte count = e.type() == GdkEventType.BUTTON_PRESS ? 1 : 
			e.type() == GdkEventType.DOUBLE_BUTTON_PRESS ? 2 : 3;
		onMouseDown(count, gdkButtonToMouseButton(btn.button), gdkStateToModifierKey(btn.state));
		return true;
	}

	bool onButtonRelease(Event e, Widget) @trusted
	in { assert(e.type() == GdkEventType.BUTTON_RELEASE); }
	body
	{//log("onButtonRelease");
		m_onMouseAction(e, MouseAction.motion);//FIXME

		const btn = e.button();
		onMouseUp(gdkButtonToMouseButton(btn.button), gdkStateToModifierKey(btn.state));
		return true;
	}

	bool onScroll(Event e, Widget) @trusted
	in
	{
		assert(e.type() == GdkEventType.SCROLL);
		assert(e.scroll().direction != GdkScrollDirection.SMOOTH); // As we don't set GDK_SMOOTH_SCROLL_MASK
	}
	body
	{
		m_onMouseAction(e, MouseAction.motion);//FIXME

		const scroll = e.scroll();
		onMouseScroll(gdkScrollDirectionToScrollDirection(scroll.direction), gdkStateToModifierKey(scroll.state));
		return true;
	}

	bool onDraw(Context context, Widget) @trusted
	in { assert(cairoDrawer); }
	body
	{
		// Workaround GtkD @@@BUG35@@@
		// Warning: This fix is for GtkD 2.3.0. Carefully check GtkD
		// changes and update implementation if needed to prevent
		// delayed program crash.
		static assert(is(typeof(context.tupleof[0]) == cairo_t*));
		import gtkc.cairo;
		scope(exit)
		{
			cairo_destroy(context.tupleof[0]);
			context.tupleof[0] = null;
		}

		cairoDrawer.setContext(context);
		super.onDraw();
		return true;
	}
}

private:

MouseButton gdkButtonToMouseButton(uint button)
{
	switch(button)
	{
		case 1 /* GDK_BUTTON_PRIMARY */: return MouseButton.left;
		case 2 /* GDK_BUTTON_MIDDLE */: return MouseButton.middle;
		case 3 /* GDK_BUTTON_SECONDARY */: return MouseButton.right;
		case 4: return MouseButton.x1;
		case 5: return MouseButton.x2;
		default: return MouseButton.other;
	}
}


alias document.interfaces.ScrollDirection ScrollDirection;

ScrollDirection gdkScrollDirectionToScrollDirection(GdkScrollDirection direction)
{
	switch(direction)
	{
		case GdkScrollDirection.UP:     return ScrollDirection.up;
		case GdkScrollDirection.DOWN:   return ScrollDirection.down;
		case GdkScrollDirection.LEFT:   return ScrollDirection.left;
		case GdkScrollDirection.RIGHT:  return ScrollDirection.right;
		default: assert(0);
	}
}


Point getClientPointer(Widget w, out GdkModifierType modifierType) @trusted
{
	import gtkc.gdk;
	import gtkc.gtk;
	GdkWindow* gdkWindow = gtk_widget_get_window(w.getWidgetStruct());
	GdkDeviceManager* manager = gdk_display_get_device_manager(gdk_display_get_default());
	GdkDevice* device = gdk_device_manager_get_client_pointer(manager);

	gint cx, cy;
	gdk_window_get_device_position(gdkWindow, device, &cx, &cy, &modifierType);

	return Point(cx, cy);
}
