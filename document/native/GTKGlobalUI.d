﻿/**
DocumentFramework project project file.

Copyright: Denis Shelomovskij 2013

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module document.native.GTKGlobalUI;


version(GTK3_Tahoma_8_font) import std.string; // for toStringz

import unstd.casts;

import metaui.controls.native;

import gtkc.gdk;
import gtkc.gtk;
version(GTK3_Tahoma_8_font) import gtkc.gtktypes;

import gtk.Window;

static import gtkd.gtk.controls;

import document.utils;
import document.GlobalUI;


@safe:

final class GTKGlobalUI: GlobalUI
{
	// Constructor
	// ----------------------------------------------------------------------------------------------------

	this(WindowFactory windowFactory)
	{
		super(windowFactory);
	}

protected:

	// Abstract function implementation
	// ----------------------------------------------------------------------------------------------------

	final override
	{
		void initializeNative() @trusted
		{
			debug(gtk)
			{
				import gtk.Version;
				import std.stdio;
				writefln("GTK version: %s.%s.%s", Version.getMajorVersion(), Version.getMinorVersion(), Version.getMicroVersion());
			}
			gtk_init(null, null);
			version(GTK3_Tahoma_8_font)
				loadCssFromData("*{font:Tahoma 8;}");
		}

		void runNative() @trusted
		{ gtk_main(); } // run main loop

		void quitNative() @trusted
		{ gtk_main_quit(); }


		// Dialogs run functions
		// --------------------------------------------------

		bool runOpenSaveDialog(INativeControl control,
			in bool openDialog, string title,
			in FileExtension[] extensions,
			in string currentPath, in string currentFolder, in string currentName,
			out string path) @trusted
		in { assert(!(currentPath && (currentFolder || currentName))); }
		body
		{
			import std.algorithm;

			import gtk.FileChooserDialog;
			import gtk.FileFilter;

			__gshared static string[] texts = ["gtk-ok", "gtk-cancel"];
			auto dialog = new FileChooserDialog(
				title,
				getWindow(control),
				openDialog ? FileChooserAction.OPEN : FileChooserAction.SAVE,
				texts);
			scope(exit) dialog.destroy();

			if(!openDialog)
				dialog.setDoOverwriteConfirmation(true);

			string[void*] extsByFilter;
			FileFilter filterDefault = null;

			if(openDialog && extensions.length > 1)
			{
				auto filter = new FileFilter();
				filter.setName(loc(
					"Все поддерживаемые файлы (%s)",
					"All supported files (%s)",
					format("%-(*.%s;%)", extensions.map!`a.ext`())));

				foreach(const extension; extensions)
					filter.addPattern("*." ~ extension.ext);

				extsByFilter[cast(void*) filter] = null;
				dialog.addFilter(filter);
				filterDefault = filter;
			}
			
			foreach(i, extension; extensions)
			{
				auto filter = new FileFilter();
				string pattern = "*." ~ extension.ext;
				filter.setName(loc("%s (%s)", "%s (%s)", extension.title, pattern));
				filter.addPattern(pattern);
				extsByFilter[cast(void*) filter] = extension.ext;

				dialog.addFilter(filter);
				if(!filterDefault)
					filterDefault = filter;
			}

			auto filterAll = new FileFilter();
			filterAll.setName(loc("Все файлы", "All files"));
			filterAll.addPattern("*");
			extsByFilter[cast(void*) filterAll] = null;

			dialog.addFilter(filterAll);
			dialog.setFilter(filterDefault);
			string lastExt = extsByFilter[cast(void*) filterDefault];
			dialog.addOnNotify((spec, s)
			{
				import std.path, std.string;

				const fileName = dialog.getFilename().baseName();
				const ext = fileName.extension();
				const needUpdateExt = ext.length <= 1 || icmp(ext[1 .. $], lastExt) == 0;
				lastExt = extsByFilter[cast(void*) dialog.getFilter()];
				if(needUpdateExt && lastExt)
					dialog.setCurrentName(fileName.setExtension(lastExt));
			}, "filter");

			if(currentPath)
				dialog.setFilename(currentPath);
			if(currentFolder)
				dialog.setCurrentFolder(currentFolder);
			if(currentName)
				dialog.setCurrentName(currentName ~ '.' ~ extensions[0].ext);

			for(;;)
			{
				if(dialog.run() == ResponseType.OK)
				{
					static import std.file;

					path = dialog.getFilename();
					if(openDialog && !std.file.exists(path))
						continue;
					return true;
				}
				return false;
			}
		}

		void runInformationDialog(INativeControl control, string text)
		{
			_inf(getWindow(control), text);
		}

		bool runQuestionDialog(INativeControl control, string text, string secondaryText, bool defaultResponse)
		{
			return ask(getWindow(control), text, secondaryText, defaultResponse ? GtkResponseType.YES : GtkResponseType.NO);
		}

		Question3Res runQuestion3Dialog(INativeControl control, string text, string secondaryText, Question3Res defaultResponse)
		{
			return ask3(getWindow(control), text, secondaryText, defaultResponse == Question3Res.yes ? GtkResponseType.YES : 
				defaultResponse == Question3Res.no ? GtkResponseType.NO : GtkResponseType.CANCEL);
		}

		void runErrorDialog(INativeControl control, string text, string secondaryText)
		{
			_err(control ? getWindow(control) : null, text, secondaryText);
		}
	}

private:

	static Window getWindow(INativeControl control) @trusted
	in { assert(control); }
	body
	{
		import gobject.ObjectG;

		auto widget = control.dynamicCast!(gtkd.gtk.controls.Widget);
		auto gtkToplevelWidget = widget.entity.gtk_widget_get_toplevel();
		assert(gtkToplevelWidget.gtk_widget_is_toplevel());
		
		return ObjectG.getDObject!Window(cast(GtkWindow*) gtkToplevelWidget);
	}

	// Msgr
	import gtk.MessageDialog;

	enum MessageType IconErr = MessageType.ERROR, IconQuest = MessageType.QUESTION, IconWarn = MessageType.WARNING, IconInf = MessageType.INFO;
	enum DialogFlags f = DialogFlags.MODAL | DialogFlags.DESTROY_WITH_PARENT;

	private static void setSecondaryText(MessageDialog md, string secondaryText) @trusted
	{
		import glib.Str;

		const char* cSecText = secondaryText.length ? Str.toStringz(secondaryText) : cast(const char*) "";
		gtk_message_dialog_format_secondary_text(md.getMessageDialogStruct(), cast(char*) "%s", cSecText);
	}

	private static MessageDialog newMessageDialog(Window parent, GtkDialogFlags flags, GtkMessageType type, GtkButtonsType buttons, string message) @trusted
	{
		import glib.Str;
		import glib.ConstructionException;

		const char* cMsg = message.length ? Str.toStringz(message) : cast(const char*) "";
		if(auto p = cast(GtkMessageDialog*) gtk_message_dialog_new(
			parent ? parent.getWindowStruct() : null, flags, type, buttons, cast(char*) "%s", cMsg))
			return new MessageDialog(p);

		throw new ConstructionException("null returned by gtk_message_dialog_new()");
	}

	void show(Window window, string text, string secondaryText, MessageType type) @trusted
	{
		MessageDialog md = newMessageDialog(window, f, type, ButtonsType.OK, text);
		setSecondaryText(md, secondaryText);
		md.run();
		md.destroy();
	}

	public /*FIXME*/bool ask(Window window, string text, string secondaryText = null, GtkResponseType defaultResponse = GtkResponseType.YES, MessageType type = IconQuest) @trusted
	{
		MessageDialog md = newMessageDialog(window, f, type, ButtonsType.YES_NO, text);
		if(secondaryText)
			setSecondaryText(md, secondaryText);
		md.setDefaultResponse(defaultResponse);
		int res = md.run();
		md.destroy();
		return res == GtkResponseType.YES;
	}

	Question3Res ask3(Window window, string text, string secondaryText, GtkResponseType defaultResponse = GtkResponseType.YES, MessageType type = IconQuest) @trusted
	{
		MessageDialog md = newMessageDialog(window, f, type, ButtonsType.YES_NO, text);
		setSecondaryText(md, secondaryText);
		md.addButton("gtk-cancel", GtkResponseType.CANCEL);
		md.setDefaultResponse(defaultResponse);
		int res = md.run();
		md.destroy();
		with(GtkResponseType) switch(res)
		{
			case YES: return Question3Res.yes;
			case NO: return Question3Res.no;
			default: return Question3Res.cancel;
		}
	}

	public /*FIXME*/void _err(Window window, string text, string secondaryText) { show(window, text, secondaryText, IconErr); }

	bool _quest(Window window, string t) { return ask(window, loc("Вопрос", "Question"), t, GtkResponseType.YES, IconQuest); }

	void _warn(Window window, string text, string secondaryText) { show(window, text, secondaryText, IconWarn); }

	void _warn(Window window, string t) { _warn(window, loc("Предупреждение", "Warning"), t); }

	public /*FIXME*/ void _inf(Window window, string t) { show(window, loc("Информация", "Information"), t, IconInf); }

	bool _askErr(Window window, string t) { return ask(window, loc("Ошибка", "Error"), t, GtkResponseType.YES, IconErr); }
}

version(GTK3_Tahoma_8_font) private:

GtkCssProvider* createDefaultCssProvider() @trusted
{
	enum GTK_STYLE_PROVIDER_PRIORITY_APPLICATION = 600;
	GtkCssProvider* provider = gtk_css_provider_new();
	GdkDisplay* display = gdk_display_get_default();
	GdkScreen* screen = gdk_display_get_default_screen(display);
	gtk_style_context_add_provider_for_screen(screen, cast(GtkStyleProvider*) provider, GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
	return provider;
}

void loadCssFromPath(string path) @trusted
{
	gtk_css_provider_load_from_path(createDefaultCssProvider(), cast(gchar*) toStringz(path), null);
}

void loadCssFromData(string data) @trusted
{
	gtk_css_provider_load_from_data(createDefaultCssProvider(), cast(gchar*) data.ptr, data.length, null);
}
