﻿/**
DocumentFramework project project file.

Copyright: Denis Shelomovskij 2013

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module document.utils;


public import unstdexp.utils;
public import unstdexp.collections.orderedset;


string loc(T...)(string russianFormat, string englishFormat, T args)
{
	import std.string: format;
	static if(args.length)
		return format(russianFormat, args);
	else
		return russianFormat;
}

// Tracing function
// ----------------------------------------------------------------------------------------------------

debug void tr(string file = __FILE__, size_t line = __LINE__, A...)(A msgs) @trusted
{
	import std.stdio;
	static ulong n = 0;
	writef("%s@%s(%s)", file, line, ++n);
	static if(A.length == 0)
	{
		writeln(" reached");
	}
	else
	{
		write(':');
		foreach(ref msg; msgs)
			write(' ', msg);
		writeln();
	}
}
