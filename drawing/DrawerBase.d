﻿/**
DocumentFramework project project file.

Copyright: Denis Shelomovskij 2013

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module drawing.DrawerBase;


import std.string;

import geometry.in2d.box;
import geometry.in2d.point;
import geometry.in2d.vector;
import geometry.in2d.curves.segment;
import geometry.in2d.curves.circle;
import geometry.in2d.curves.circulararc;

import drawing.color;


@safe:

struct TextExtents
{
	double xBearing, yBearing;
	double width, height;
	double xAdvance, yAdvance;
}

enum VerticalAlignment
{
	baseline,
	bottom,
	middle,
	top,
}

enum HorizontalAlignment
{
	left,
	centre,
	right,
}

interface IDrawerBase
{
	void finish();

	@property
	{
		void color(in Color c);
		Color color() const;
		void antialias(in bool val);
		void lineWidth(in double val);

		void fontSize(in double val);
		void fontLineHeight(in double val);
		protected double fontLineHeight() const pure nothrow @nogc;
		void fontFace(in string val);
	}

	void translate(in Vector v);
	void scale(in double sx, in double sy);

	final IDrawerBase withColor(in Color c)
	{
		color = c;
		return this;
	}

	void clear(in Color c);

	void stroke(in Segment seg);
	void stroke(in Circle circle);
	void fill(in Circle circle);
	void stroke(in CircularArc arc);
	void fillCircularSector(in Circle circle, in double startAngle, in double endAngle);
	void strokePolygonalChain(in Point[] points...);
	void strokePolygon(in Point[] points...);
	void fillPolygon(in Point[] points...);

	void fillAlternatingCircularSectors(in Circle circle, in double startAngle, in int sectors);

	final void stroke(in Box box)
	{
		const p1 = box.start, p2 = box.end;
		strokePolygon(p1, Point(p1.x, p2.y), p2, Point(p2.x, p1.y));
	}

	final void fill(in Box box)
	{
		const p1 = box.start, p2 = box.end;
		fillPolygon(p1, Point(p1.x, p2.y), p2, Point(p2.x, p1.y));
	}

	final void fill(in Segment seg, in double distance)
	{
		const v = seg.vector.normal.withLength(distance);
		fillPolygon
		(
			seg.start - v,
			seg.start + v,
			seg.end + v,
			seg.end - v,
		);
	}

	void fillMoveTo(in Point point);
	void fillAddSegmentTo(in Point point);
	void fillAdd(in CircularArc arc);
	void fillAdded();

	final void showText(in Point p, in string text,
		in VerticalAlignment verticalAlignment, in HorizontalAlignment horizontalAlignment)
	{
		const lines = text.splitLines();
		if(lines.length == 1)
		{
			showTextLine(p, lines[0], verticalAlignment, horizontalAlignment);
			return;
		}

		const lineHeight = fontLineHeight;

		Point p2 = p;

		if(verticalAlignment == VerticalAlignment.bottom)
			p2.y -= (lines.length - 1) * lineHeight;
		else if(verticalAlignment == VerticalAlignment.middle)
			p2.y -= (lines.length - 1) * lineHeight / 2;

		foreach(const line; lines)
		{
			showTextLine(p2, line, verticalAlignment, horizontalAlignment);
			p2.y += lineHeight;
		}
	}

	protected void showTextLine(in Point p, in string text,
		in VerticalAlignment verticalAlignment, in HorizontalAlignment horizontalAlignment);

	void dropShadow(in Segment seg, in double d, in Color color);
	void dropShadow(in Circle circle, in double d, in Color color);
	void dropShadow(in CircularArc arc, in double d, in Color color);
	void dropShadow(in Box box, in double d, in Color color);
}
