/**
DocumentFramework project project file.

Copyright: Denis Shelomovskij 2013

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module drawing.color;


import std.algorithm; // min, max
import std.conv; // octal, text
import std.exception; // assumeWontThrow
import std.traits; // isNumeric

import drawing.colors;


@safe pure nothrow:

struct Color
{
pure nothrow:

	// Fields
	// ----------------------------------------------------------------------------------------------------

	ubyte r, g, b, a = ubyte.max;

	// Constructors
	// ----------------------------------------------------------------------------------------------------

	this(ubyte reg, ubyte green, ubyte blue, ubyte alpha = ubyte.max)
	{ this.r = reg; this.g = green; this.b = blue; this.a = alpha; }

	this(uint rgbaValue)
	{
		a = rgbaValue & 0xFF; rgbaValue >>= octal!10;
		b = rgbaValue & 0xFF; rgbaValue >>= octal!10;
		g = rgbaValue & 0xFF; rgbaValue >>= octal!10;
		r = rgbaValue & 0xFF; assert(!(rgbaValue >> octal!10));
	}

	this(Color c, ubyte alpha)
	{ r = c.r; g = c.g; b = c.b; a = alpha; }

	// Construction functions
	// ----------------------------------------------------------------------------------------------------

	static
	{
		Color mix(T)(Color c1, T val1, Color c2, T val2)
			if(isNumeric!T)
		in { assert(val1 >= 0 && val2 >= 0); }
		body {
			T val = val1 + val2;
			return Color(
				cast(ubyte) ((c1.r * val1 + c2.r * val2) / val),
				cast(ubyte) ((c1.g * val1 + c2.g * val2) / val),
				cast(ubyte) ((c1.b * val1 + c2.b * val2) / val),
				cast(ubyte) ((c1.a * val1 + c2.a * val2) / val)
			);
		}
	}

	// Properties
	// ----------------------------------------------------------------------------------------------------

	const @property
	{
		real opacity()
		{ return cast(real) a / ubyte.max; }

		int rgbaValue()
		{ return r << octal!30 | g << octal!20 | b << octal!10 | a; }

		ubyte rgbIntensity()
		{ return cast(ubyte) ((r + g + b) / 3); }

		Color grayscaled()
		{
			immutable i = rgbIntensity;
			return Color(i, i, i, a);
		}
	}

	@property
	{
		void opacity(real val)
		in { assert(val >= 0 && val <= 1); }
		body { a = cast(ubyte) (val * ubyte.max); }
	}

	// Operators
	// ----------------------------------------------------------------------------------------------------

	const
	{//TODO
		Color opAdd(Color c)
		{ return adjustCreate!int(r + c.r, g + c.g, b + c.b, adjust!int(a + c.a)); }

		Color opMul(T)(T n)
		{ return adjustCreate!T(r * n, g * n, b * n, a); }

		Color opDiv(T)(T n)
		{ return adjustCreate!T(r / n, g / n, b / n, a); }
	}

	// Functions
	// ----------------------------------------------------------------------------------------------------

	const
	{
		string toString()
		{
			if(const name = NamedColors.getName(Color(this, ubyte.max)))
			{
				if(a == ubyte.max)
					return text("{", name, "}").assumeWontThrow();
				else
					return text("{", name, ", a:", a, "}").assumeWontThrow();
			}
			return text("{r:", r, ", g:", g, ", b:", b, ", a:", a, "}").assumeWontThrow();
		}
	}

	private static
	{
		ubyte adjust(T)(T t)
		{
			return cast(ubyte) min(cast(T) ubyte.max, max(cast(T) 0, t));
		}

		Color adjustCreate(T)(T r, T g, T b, ubyte a)
		{
			return Color(adjust(r), adjust(g), adjust(b), a);
		}
	}
}

unittest
{
	auto c = Color(1, 2, 3);
	assert(c == Color(1, 2, 3, ubyte.max));
	assert(c.opacity == 1);
	assert(c.rgbaValue == 0x010203FF);
	assert(Color(c.rgbaValue) == c);
	assert(c.rgbIntensity == 2);
	assert(c.grayscaled == Color(2, 2, 2));
	assert(c * real.nan == Color(0, 0, 0));

	auto c2 = Color(c, ubyte.max / 2);
	assert(c2.opacity == cast(real) (ubyte.max / 2) / ubyte.max);
	assert(c2.rgbaValue == 0x0102037F);
	assert(Color(c2.rgbaValue) == c2);
	assert(c2.rgbIntensity == c.rgbIntensity);
	assert(c2.grayscaled == Color(2, 2, 2, c2.a));
}
