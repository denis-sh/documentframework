/**
DocumentFramework project project file.

Copyright: Denis Shelomovskij 2013

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module drawing.colors;


import std.exception; // assumeWontThrow
import std.string; // toLower

import drawing.color;


@safe:

struct NamedColors
{
pure nothrow:

static:
	immutable(Color)* getByName(in char[] name)
	{
		return name.toLower().assumeWontThrow() in colorsByLowerCaseNames;
	}

	string getName(in Color color)
	{
		if(color.a != ubyte.max)
			return null;

		auto name = color in camelCaseNames;
		return name ? *name : null;
	}


	enum Color
		aliceBlue				= Color(240, 248, 255),		// -984833
		antiqueWhite			= Color(250, 235, 215),		// -332841
		aqua					= Color(0, 255, 255),		// -16711681
		aquamarine				= Color(127, 255, 212),		// -8388652
		azure					= Color(240, 255, 255),		// -983041
		beige					= Color(245, 245, 220),		// -657956
		bisque					= Color(255, 228, 196),		// -6972
		black					= Color(0,   0,   0),		// -16777216
		blanchedAlmond			= Color(255, 235, 205),		// -5171
		blue					= Color(0,   0, 255),		// -16776961
		blueViolet				= Color(138,  43, 226),		// -7722014
		brown					= Color(165,  42,  42),		// -5952982
		burlyWood				= Color(222, 184, 135),		// -2180985
		cadetBlue				= Color(95, 158, 160),		// -10510688
		chartreuse				= Color(127, 255,   0),		// -8388864
		chocolate				= Color(210, 105,  30),		// -2987746
		coral					= Color(255, 127,  80),		// -32944
		cornflowerBlue			= Color(100, 149, 237),		// -10185235
		cornsilk				= Color(255, 248, 220),		// -1828
		crimson					= Color(220,  20,  60),		// -2354116
		cyan					= Color(0, 255, 255),		// -16711681
		darkBlue				= Color(0,   0, 139),		// -16777077
		darkCyan				= Color(0, 139, 139),		// -16741493
		darkGoldenrod			= Color(184, 134,  11),		// -4684277
		darkGray				= Color(169, 169, 169),		// -5658199
		darkGreen				= Color(0, 100,   0),		// -16751616
		darkKhaki				= Color(189, 183, 107),		// -4343957
		darkMagenta				= Color(139,   0, 139),		// -7667573
		darkOliveGreen			= Color(85, 107,  47),		// -11179217
		darkOrange				= Color(255, 140,   0),		// -29696
		darkOrchid				= Color(153,  50, 204),		// -6737204
		darkRed					= Color(139,   0,   0),		// -7667712
		darkSalmon				= Color(233, 150, 122),		// -1468806
		darkSeaGreen			= Color(143, 188, 139),		// -7357301
		darkSlateBlue			= Color(72,  61, 139),		// -12042869
		darkSlateGray			= Color(47,  79,  79),		// -13676721
		darkTurquoise			= Color(0, 206, 209),		// -16724271
		darkViolet				= Color(148,   0, 211),		// -7077677
		deepPink				= Color(255,  20, 147),		// -60269
		deepSkyBlue				= Color(0, 191, 255),		// -16728065
		dimGray					= Color(105, 105, 105),		// -9868951
		dodgerBlue				= Color(30, 144, 255),		// -14774017
		firebrick				= Color(178,  34,  34),		// -5103070
		floralWhite				= Color(255, 250, 240),		// -1296
		forestGreen				= Color(34, 139,  34),		// -14513374
		fuchsia					= Color(255,   0, 255),		// -65281
		gainsboro				= Color(220, 220, 220),		// -2302756
		ghostWhite				= Color(248, 248, 255),		// -460545
		gold					= Color(255, 215,   0),		// -10496
		goldenrod				= Color(218, 165,  32),		// -2448096
		gray					= Color(128, 128, 128),		// -8355712
		green					= Color(0, 128,   0),		// -16744448
		greenYellow				= Color(173, 255,  47),		// -5374161
		honeydew				= Color(240, 255, 240),		// -983056
		hotPink					= Color(255, 105, 180),		// -38476
		indianRed				= Color(205,  92,  92),		// -3318692
		indigo					= Color(75,   0, 130),		// -11861886
		ivory					= Color(255, 255, 240),		// -16
		khaki					= Color(240, 230, 140),		// -989556
		lavender				= Color(230, 230, 250),		// -1644806
		lavenderBlush			= Color(255, 240, 245),		// -3851
		lawnGreen				= Color(124, 252,   0),		// -8586240
		lemonChiffon			= Color(255, 250, 205),		// -1331
		lightBlue				= Color(173, 216, 230),		// -5383962
		lightCoral				= Color(240, 128, 128),		// -1015680
		lightCyan				= Color(224, 255, 255),		// -2031617
		lightGoldenrodYellow	= Color(250, 250, 210),		// -329006
		lightGray				= Color(211, 211, 211),		// -2894893
		lightGreen				= Color(144, 238, 144),		// -7278960
		lightPink				= Color(255, 182, 193),		// -18751
		lightSalmon				= Color(255, 160, 122),		// -24454
		lightSeaGreen			= Color(32, 178, 170),		// -14634326
		lightSkyBlue			= Color(135, 206, 250),		// -7876870
		lightSlateGray			= Color(119, 136, 153),		// -8943463
		lightSteelBlue			= Color(176, 196, 222),		// -5192482
		lightYellow				= Color(255, 255, 224),		// -32
		lime					= Color(0, 255,   0),		// -16711936
		limeGreen				= Color(50, 205,  50),		// -13447886
		linen					= Color(250, 240, 230),		// -331546
		magenta					= Color(255,   0, 255),		// -65281
		maroon					= Color(128,   0,   0),		// -8388608
		mediumAquamarine		= Color(102, 205, 170),		// -10039894
		mediumBlue				= Color(0,   0, 205),		// -16777011
		mediumOrchid			= Color(186,  85, 211),		// -4565549
		mediumPurple			= Color(147, 112, 219),		// -7114533
		mediumSeaGreen			= Color(60, 179, 113),		// -12799119
		mediumSlateBlue			= Color(123, 104, 238),		// -8689426
		mediumSpringGreen		= Color(0, 250, 154),		// -16713062
		mediumTurquoise			= Color(72, 209, 204),		// -12004916
		mediumVioletRed			= Color(199,  21, 133),		// -3730043
		midnightBlue			= Color(25,  25, 112),		// -15132304
		mintCream				= Color(245, 255, 250),		// -655366
		mistyRose				= Color(255, 228, 225),		// -6943
		moccasin				= Color(255, 228, 181),		// -6987
		navajoWhite				= Color(255, 222, 173),		// -8531
		navy					= Color(0,   0, 128),		// -16777088
		oldLace					= Color(253, 245, 230),		// -133658
		olive					= Color(128, 128,   0),		// -8355840
		oliveDrab				= Color(107, 142,  35),		// -9728477
		orange					= Color(255, 165,   0),		// -23296
		orangeRed				= Color(255,  69,   0),		// -47872
		orchid					= Color(218, 112, 214),		// -2461482
		paleGoldenrod			= Color(238, 232, 170),		// -1120086
		paleGreen				= Color(152, 251, 152),		// -6751336
		paleTurquoise			= Color(175, 238, 238),		// -5247250
		paleVioletRed			= Color(219, 112, 147),		// -2396013
		papayaWhip				= Color(255, 239, 213),		// -4139
		peachPuff				= Color(255, 218, 185),		// -9543
		peru					= Color(205, 133,  63),		// -3308225
		pink					= Color(255, 192, 203),		// -16181
		plum					= Color(221, 160, 221),		// -2252579
		powderBlue				= Color(176, 224, 230),		// -5185306
		purple					= Color(128,   0, 128),		// -8388480
		red						= Color(255,   0,   0),		// -65536
		rosyBrown				= Color(188, 143, 143),		// -4419697
		royalBlue				= Color(65, 105, 225),		// -12490271
		saddleBrown				= Color(139,  69,  19),		// -7650029
		salmon					= Color(250, 128, 114),		// -360334
		sandyBrown				= Color(244, 164,  96),		// -744352
		seaGreen				= Color(46, 139,  87),		// -13726889
		seaShell				= Color(255, 245, 238),		// -2578
		sienna					= Color(160,  82,  45),		// -6270419
		silver					= Color(192, 192, 192),		// -4144960
		skyBlue					= Color(135, 206, 235),		// -7876885
		slateBlue				= Color(106,  90, 205),		// -9807155
		slateGray				= Color(112, 128, 144),		// -9404272
		snow					= Color(255, 250, 250),		// -1286
		springGreen				= Color(0, 255, 127),		// -16711809
		steelBlue				= Color(70, 130, 180),		// -12156236
		tan						= Color(210, 180, 140),		// -2968436
		teal					= Color(0, 128, 128),		// -16744320
		thistle					= Color(216, 191, 216),		// -2572328
		tomato					= Color(255,  99,  71),		// -40121
		transparent				= Color(255, 255, 255),		// 16777215
		turquoise				= Color(64, 224, 208),		// -12525360
		violet					= Color(238, 130, 238),		// -1146130
		wheat					= Color(245, 222, 179),		// -663885
		white					= Color(255, 255, 255),		// -1
		whiteSmoke				= Color(245, 245, 245),		// -657931
		yellow					= Color(255, 255,   0),		// -256
		yellowGreen				= Color(154, 205,  50)		// -6632142
	;


	immutable string[Color] camelCaseNames;

	immutable Color[string] colorsByLowerCaseNames;

	@trusted shared static this() // `rehash` is system, see dmd @@@BUG6357@@@
	{
		camelCaseNames =
		[
			aliceBlue				:"AliceBlue",
			antiqueWhite			:"AntiqueWhite",
			aqua					:"Aqua",
			aquamarine				:"Aquamarine",
			azure					:"Azure",
			beige					:"Beige",
			bisque					:"Bisque",
			black					:"Black",
			blanchedAlmond			:"BlanchedAlmond",
			blue					:"Blue",
			blueViolet				:"BlueViolet",
			brown					:"Brown",
			burlyWood				:"BurlyWood",
			cadetBlue				:"CadetBlue",
			chartreuse				:"Chartreuse",
			chocolate				:"Chocolate",
			coral					:"Coral",
			cornflowerBlue			:"CornflowerBlue",
			cornsilk				:"Cornsilk",
			crimson					:"Crimson",
			cyan					:"Cyan",
			darkBlue				:"DarkBlue",
			darkCyan				:"DarkCyan",
			darkGoldenrod			:"DarkGoldenrod",
			darkGray				:"DarkGray",
			darkGreen				:"DarkGreen",
			darkKhaki				:"DarkKhaki",
			darkMagenta				:"DarkMagenta",
			darkOliveGreen			:"DarkOliveGreen",
			darkOrange				:"DarkOrange",
			darkOrchid				:"DarkOrchid",
			darkRed					:"DarkRed",
			darkSalmon				:"DarkSalmon",
			darkSeaGreen			:"DarkSeaGreen",
			darkSlateBlue			:"DarkSlateBlue",
			darkSlateGray			:"DarkSlateGray",
			darkTurquoise			:"DarkTurquoise",
			darkViolet				:"DarkViolet",
			deepPink				:"DeepPink",
			deepSkyBlue				:"DeepSkyBlue",
			dimGray					:"DimGray",
			dodgerBlue				:"DodgerBlue",
			firebrick				:"Firebrick",
			floralWhite				:"FloralWhite",
			forestGreen				:"ForestGreen",
			fuchsia					:"Fuchsia",
			gainsboro				:"Gainsboro",
			ghostWhite				:"GhostWhite",
			gold					:"Gold",
			goldenrod				:"Goldenrod",
			gray					:"Gray",
			green					:"Green",
			greenYellow				:"GreenYellow",
			honeydew				:"Honeydew",
			hotPink					:"HotPink",
			indianRed				:"IndianRed",
			indigo					:"Indigo",
			ivory					:"Ivory",
			khaki					:"Khaki",
			lavender				:"Lavender",
			lavenderBlush			:"LavenderBlush",
			lawnGreen				:"LawnGreen",
			lemonChiffon			:"LemonChiffon",
			lightBlue				:"LightBlue",
			lightCoral				:"LightCoral",
			lightCyan				:"LightCyan",
			lightGoldenrodYellow	:"LightGoldenrodYellow",
			lightGray				:"LightGray",
			lightGreen				:"LightGreen",
			lightPink				:"LightPink",
			lightSalmon				:"LightSalmon",
			lightSeaGreen			:"LightSeaGreen",
			lightSkyBlue			:"LightSkyBlue",
			lightSlateGray			:"LightSlateGray",
			lightSteelBlue			:"LightSteelBlue",
			lightYellow				:"LightYellow",
			lime					:"Lime",
			limeGreen				:"LimeGreen",
			linen					:"Linen",
			magenta					:"Magenta",
			maroon					:"Maroon",
			mediumAquamarine		:"MediumAquamarine",
			mediumBlue				:"MediumBlue",
			mediumOrchid			:"MediumOrchid",
			mediumPurple			:"MediumPurple",
			mediumSeaGreen			:"MediumSeaGreen",
			mediumSlateBlue			:"MediumSlateBlue",
			mediumSpringGreen		:"MediumSpringGreen",
			mediumTurquoise			:"MediumTurquoise",
			mediumVioletRed			:"MediumVioletRed",
			midnightBlue			:"MidnightBlue",
			mintCream				:"MintCream",
			mistyRose				:"MistyRose",
			moccasin				:"Moccasin",
			navajoWhite				:"NavajoWhite",
			navy					:"Navy",
			oldLace					:"OldLace",
			olive					:"Olive",
			oliveDrab				:"OliveDrab",
			orange					:"Orange",
			orangeRed				:"OrangeRed",
			orchid					:"Orchid",
			paleGoldenrod			:"PaleGoldenrod",
			paleGreen				:"PaleGreen",
			paleTurquoise			:"PaleTurquoise",
			paleVioletRed			:"PaleVioletRed",
			papayaWhip				:"PapayaWhip",
			peachPuff				:"PeachPuff",
			peru					:"Peru",
			pink					:"Pink",
			plum					:"Plum",
			powderBlue				:"PowderBlue",
			purple					:"Purple",
			red						:"Red",
			rosyBrown				:"RosyBrown",
			royalBlue				:"RoyalBlue",
			saddleBrown				:"SaddleBrown",
			salmon					:"Salmon",
			sandyBrown				:"SandyBrown",
			seaGreen				:"SeaGreen",
			seaShell				:"SeaShell",
			sienna					:"Sienna",
			silver					:"Silver",
			skyBlue					:"SkyBlue",
			slateBlue				:"SlateBlue",
			slateGray				:"SlateGray",
			snow					:"Snow",
			springGreen				:"SpringGreen",
			steelBlue				:"SteelBlue",
			tan						:"Tan",
			teal					:"Teal",
			thistle					:"Thistle",
			tomato					:"Tomato",
			transparent				:"Transparent",
			turquoise				:"Turquoise",
			violet					:"Violet",
			wheat					:"Wheat",
			white					:"White",
			whiteSmoke				:"WhiteSmoke",
			yellow					:"Yellow",
			yellowGreen				:"YellowGreen"			   
		];
		(cast() camelCaseNames).rehash();

		colorsByLowerCaseNames =
		[
			"aliceblue"	        	:	aliceBlue			,
			"antiquewhite"        	:	antiqueWhite		,
			"aqua"                	:	aqua				,
			"aquamarine"          	:	aquamarine			,
			"azure"               	:	azure				,
			"beige"               	:	beige				,
			"bisque"              	:	bisque				,
			"black"               	:	black				,
			"blanchedalmond"      	:	blanchedAlmond		,
			"blue"                	:	blue				,
			"blueviolet"          	:	blueViolet			,
			"brown"               	:	brown				,
			"burlywood"           	:	burlyWood			,
			"cadetblue"           	:	cadetBlue			,
			"chartreuse"          	:	chartreuse			,
			"chocolate"           	:	chocolate			,
			"coral"               	:	coral				,
			"cornflowerblue"      	:	cornflowerBlue		,
			"cornsilk"            	:	cornsilk			,
			"crimson"             	:	crimson				,
			"cyan"                	:	cyan				,
			"darkblue"            	:	darkBlue			,
			"darkcyan"            	:	darkCyan			,
			"darkgoldenrod"       	:	darkGoldenrod		,
			"darkgray"            	:	darkGray			,
			"darkgreen"           	:	darkGreen			,
			"darkkhaki"           	:	darkKhaki			,
			"darkmagenta"         	:	darkMagenta			,
			"darkolivegreen"      	:	darkOliveGreen		,
			"darkorange"          	:	darkOrange			,
			"darkorchid"          	:	darkOrchid			,
			"darkred"             	:	darkRed				,
			"darksalmon"          	:	darkSalmon			,
			"darkseagreen"        	:	darkSeaGreen		,
			"darkslateblue"       	:	darkSlateBlue		,
			"darkslategray"       	:	darkSlateGray		,
			"darkturquoise"       	:	darkTurquoise		,
			"darkviolet"          	:	darkViolet			,
			"deeppink"            	:	deepPink			,
			"deepskyblue"         	:	deepSkyBlue			,
			"dimgray"             	:	dimGray				,
			"dodgerblue"          	:	dodgerBlue			,
			"firebrick"           	:	firebrick			,
			"floralwhite"         	:	floralWhite			,
			"forestgreen"         	:	forestGreen			,
			"fuchsia"             	:	fuchsia				,
			"gainsboro"           	:	gainsboro			,
			"ghostwhite"          	:	ghostWhite			,
			"gold"                	:	gold				,
			"goldenrod"           	:	goldenrod			,
			"gray"                	:	gray				,
			"green"               	:	green				,
			"greenyellow"         	:	greenYellow			,
			"honeydew"            	:	honeydew			,
			"hotpink"             	:	hotPink				,
			"indianred"           	:	indianRed			,
			"indigo"              	:	indigo				,
			"ivory"               	:	ivory				,
			"khaki"               	:	khaki				,
			"lavender"            	:	lavender			,
			"lavenderblush"       	:	lavenderBlush		,
			"lawngreen"           	:	lawnGreen			,
			"lemonchiffon"        	:	lemonChiffon		,
			"lightblue"           	:	lightBlue			,
			"lightcoral"          	:	lightCoral			,
			"lightcyan"           	:	lightCyan			,
			"lightgoldenrodyellow"	:	lightGoldenrodYellow,
			"lightgray"           	:	lightGray			,
			"lightgreen"          	:	lightGreen			,
			"lightpink"           	:	lightPink			,
			"lightsalmon"         	:	lightSalmon			,
			"lightseagreen"       	:	lightSeaGreen		,
			"lightskyblue"        	:	lightSkyBlue		,
			"lightslategray"      	:	lightSlateGray		,
			"lightsteelblue"      	:	lightSteelBlue		,
			"lightyellow"         	:	lightYellow			,
			"lime"                	:	lime				,
			"limegreen"           	:	limeGreen			,
			"linen"               	:	linen				,
			"magenta"             	:	magenta				,
			"maroon"              	:	maroon				,
			"mediumaquamarine"    	:	mediumAquamarine	,
			"mediumblue"          	:	mediumBlue			,
			"mediumorchid"        	:	mediumOrchid		,
			"mediumpurple"        	:	mediumPurple		,
			"mediumseagreen"      	:	mediumSeaGreen		,
			"mediumslateblue"     	:	mediumSlateBlue		,
			"mediumspringgreen"   	:	mediumSpringGreen	,
			"mediumturquoise"     	:	mediumTurquoise		,
			"mediumvioletred"     	:	mediumVioletRed		,
			"midnightblue"        	:	midnightBlue		,
			"mintcream"           	:	mintCream			,
			"mistyrose"           	:	mistyRose			,
			"moccasin"            	:	moccasin			,
			"navajowhite"         	:	navajoWhite			,
			"navy"                	:	navy				,
			"oldlace"             	:	oldLace				,
			"olive"               	:	olive				,
			"olivedrab"           	:	oliveDrab			,
			"orange"              	:	orange				,
			"orangered"           	:	orangeRed			,
			"orchid"              	:	orchid				,
			"palegoldenrod"       	:	paleGoldenrod		,
			"palegreen"           	:	paleGreen			,
			"paleturquoise"       	:	paleTurquoise		,
			"palevioletred"       	:	paleVioletRed		,
			"papayawhip"          	:	papayaWhip			,
			"peachpuff"           	:	peachPuff			,
			"peru"                	:	peru				,
			"pink"                	:	pink				,
			"plum"                	:	plum				,
			"powderblue"          	:	powderBlue			,
			"purple"              	:	purple				,
			"red"                 	:	red					,
			"rosybrown"           	:	rosyBrown			,
			"royalblue"           	:	royalBlue			,
			"saddlebrown"         	:	saddleBrown			,
			"salmon"              	:	salmon				,
			"sandybrown"          	:	sandyBrown			,
			"seagreen"            	:	seaGreen			,
			"seashell"            	:	seaShell			,
			"sienna"              	:	sienna				,
			"silver"              	:	silver				,
			"skyblue"             	:	skyBlue				,
			"slateblue"           	:	slateBlue			,
			"slategray"           	:	slateGray			,
			"snow"                	:	snow				,
			"springgreen"         	:	springGreen			,
			"steelblue"           	:	steelBlue			,
			"tan"                 	:	tan					,
			"teal"                	:	teal				,
			"thistle"             	:	thistle				,
			"tomato"              	:	tomato				,
			"transparent"         	:	transparent			,
			"turquoise"           	:	turquoise			,
			"violet"              	:	violet				,
			"wheat"               	:	wheat				,
			"white"               	:	white				,
			"whitesmoke"          	:	whiteSmoke			,
			"yellow"              	:	yellow				,
			"yellowgreen"			:	yellowGreen			   
		];
		(cast() colorsByLowerCaseNames).rehash();
	}
}
