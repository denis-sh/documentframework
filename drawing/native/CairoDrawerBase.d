﻿/**
DocumentFramework project project file.

Copyright: Denis Shelomovskij 2013

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module drawing.native.CairoDrawerBase;


static import std.math;

import geometry.in2d.box;
import geometry.in2d.point;
import geometry.in2d.vector;
import geometry.in2d.curves.segment;
import geometry.in2d.curves.circle;
import geometry.in2d.curves.circulararc;

import cairo.Context;

import drawing.color;
import drawing.native.CairoHelper;
import drawing.DrawerBase;


@safe:

class CairoDrawerBase: IDrawerBase
{
@trusted:

	private
	{
		Color _color;
		double _fontLineHeight;
		Context cr;
	}

	private void resetColor()
	{
		cr.setSourceColor(_color);
	}

	void setContext(Context context)
	in { assert(context && !cr); }
	body
	{
		cr = context;
	}

override:

	void finish()
	{
		cr = null;
	}

	@property
	{
		void color(in Color c)
		{
			cr.setSourceColor(_color = c);
		}

		Color color() const @safe pure nothrow
		{
			return _color;
		}

		void antialias(in bool val)
		{
			with(cairo_antialias_t) cr.setAntialias(val ? DEFAULT : NONE);
		}

		void lineWidth(in double val)
		{
			cr.setLineWidth(val);
		}


		void fontSize(in double val)
		{
			cr.setFontSize(val);
		}

		void fontLineHeight(in double val)
		{
			_fontLineHeight = val;
		}

		double fontLineHeight() const pure nothrow @nogc
		{
			return _fontLineHeight;
		}

		void fontFace(in string val)
		{
			cr.selectFontFace(val, cairo_font_slant_t.NORMAL, cairo_font_weight_t.NORMAL);
		}
	}

	void translate(in Vector v)
	{ cr.translate(v.tupleof); }

	void scale(in double sx, in double sy)
	{ cr.scale(sx, sy); }


	void clear(in Color c)
	{
		if(c.a != ubyte.max)
			cr.setOperator(cairo_operator_t.SOURCE);
		cr.setSourceColor(c);
		cr.paint();
		if(c.a != ubyte.max)
			cr.setOperator(cairo_operator_t.OVER);
	}


	void stroke(in Segment seg)
	{
		cr.moveToP(seg.start);
		cr.lineToP(seg.end);
		cr.stroke();
	}

	void stroke(in Circle circle)
	{
		cr.drawCircle(circle.centre, circle.radius);
	}

	void fill(in Circle circle)
	{
		cr.fillCircle(circle.centre, circle.radius);
	}

	void stroke(in CircularArc arc)
	{
		Vector v1 = arc.from, v2 = arc.to;
		if(v1.crossproduct(v2) < 0)
		{
			import std.algorithm: swap;
			swap(v1, v2);
		}
		cr.moveToP(arc.circle.centre + v1 * arc.circle.radius);
		cr.arc(arc.circle.centre.tupleof, arc.circle.radius, v1.angleSignedValueXY, v2.angleSignedValueXY);
		cr.stroke();
	}

	void fillCircularSector(in Circle circle, in double startAngle, in double endAngle)
	{
		cr.moveToP(circle.centre);
		cr.arc(circle.centre.tupleof, circle.radius, startAngle, endAngle);
		cr.fill();
	}

	void strokePolygonalChain(in Point[] points...)
	in { assert(points.length >= 2); }
	body
	{
		cr.moveToP(points[0]);
		foreach(p; points[1 .. $])
			cr.lineToP(p);
		cr.stroke();
	}

	void strokePolygon(in Point[] points...)
	in { assert(points.length >= 3); }
	body
	{
		cr.moveToP(points[0]);
		foreach(p; points[1 .. $])
			cr.lineToP(p);
		cr.closePath();
		cr.stroke();
	}

	void fillPolygon(in Point[] points...)
	in { assert(points.length >= 3); }
	body
	{
		cr.moveToP(points[0]);
		foreach(p; points[1 .. $])
			cr.lineToP(p);
		cr.fill();
	}


	void fillAlternatingCircularSectors(in Circle circle, in double startAngle, in int sectors)
	{
		assert(sectors > 0);

		real ang = -startAngle;
		const dAng = std.math.PI / sectors;
		foreach(const _; 0 .. sectors)
		{
			fillCircularSector(circle, ang - dAng, ang);
			ang -= 2 * dAng;
		}
	}


	void fillMoveTo(in Point point)
	{ cr.moveToP(point); }

	void fillAddSegmentTo(in Point point)
	{ cr.lineToP(point); }

	void fillAdd(in CircularArc arc)
	{
		Vector v1 = arc.from, v2 = arc.to;
		if(v1.crossproduct(v2) < 0)
			cr.arcNegative(arc.circle.centre.tupleof, arc.circle.radius, v1.angleSignedValueXY, v2.angleSignedValueXY);
		else
			cr.arc(arc.circle.centre.tupleof, arc.circle.radius, v1.angleSignedValueXY, v2.angleSignedValueXY);
	}

	void fillAdded()
	{ cr.fill(); }

	void showTextLine(in Point p, in string text,
		in VerticalAlignment verticalAlignment, in HorizontalAlignment horizontalAlignment)
	{
		double dy, dx;

		{
			cairo_font_extents_t fe;
			if(verticalAlignment != VerticalAlignment.baseline)
				cr.fontExtents(&fe);

			with(VerticalAlignment) final switch(verticalAlignment)
			{
				case baseline:  dy = 0; break;
				case bottom:    dy = fe.descent; break;
				case middle:    dy = fe.descent - (fe.ascent + fe.descent) / 2; break;
				case top:       dy = -fe.ascent; break;
			}
		}

		{
			cairo_text_extents_t te;
			if(horizontalAlignment != HorizontalAlignment.left)
				cr.textExtents(text, &te);

			with(HorizontalAlignment) final switch(horizontalAlignment)
			{
				case left:    dx = 0; break;
				case centre:  dx = te.xAdvance * .5; break;
				case right:   dx = te.xAdvance; break;
			}
		}

		cr.moveToP(p - Vector(dx, dy));
		cr.showText(text);
	}


	void dropShadow(in Segment seg, in double d, in Color color)
	{
		cr.drawShadow(seg, d, color);
		resetColor();
	}

	void dropShadow(in Circle circle, in double d, in Color color)
	{
		cr.drawShadow(circle.centre, circle.radius, d, color);
		resetColor();
	}

	void dropShadow(in CircularArc arc, in double d, in Color color)
	{
		cr.drawShadow(arc, d, color);
		resetColor();
	}

	void dropShadow(in Box box, in double d, in Color color)
	{
		cr.fillBorderShadow(color, box.start.tupleof, box.diagonal.tupleof, d);
		resetColor();
	}
}
