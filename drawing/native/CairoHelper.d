﻿/**
DocumentFramework project project file.

Copyright: Denis Shelomovskij 2013

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module drawing.native.CairoHelper;


import geometry.in2d.vector;
import geometry.in2d.point;
import geometry.in2d.curves.segment;
import geometry.in2d.curves.circulararc;

import drawing.color;

import cairo.Context;
import cairo.Pattern;


@safe:

/+
template expressionTuple(e...)
{ alias expressionTuple = e; }

double ubyteToDouble(ubyte n){ return cast(double) n / ubyte.max; }

template toDoubleRGBA(alias c)
{
    @property double r(){ return ubyteToDouble(c.r); }
    @property double g(){ return ubyteToDouble(c.g); }
    @property double b(){ return ubyteToDouble(c.b); }
    @property double a(){ return ubyteToDouble(c.a); }
    alias toDoubleRGBA = expressionTuple!(r, g, b, a);
}
+/

struct ColorD
{
	double r, g, b, a;

	this(in Color c) inout pure nothrow
	{
		enum max = cast(real) ubyte.max;
		r = c.r / max;
		g = c.g / max;
		b = c.b / max;
		a = c.a / max;
	}
}

@trusted:

void setSourceColor(Context cr, in Color c)
{ cr.setSourceRgba(ColorD(c).tupleof); }

void roundedRectangle(Context cr, in double x, in double y, in double w, in double h, in double r)
{
	cr.moveTo(x + r, y);
	cr.lineTo(x + w - r, y);
	cr.curveTo(x + w, y, x + w, y, x + w, y + r);
	cr.lineTo(x + w, y + h - r);
	cr.curveTo(x + w, y + h, x + w, y + h, x + w - r, y + h);
	cr.lineTo(x + r, y + h);
	cr.curveTo(x, y + h, x, y + h, x, y + h - r);
	cr.lineTo(x, y + r);
	cr.curveTo(x, y, x, y, x + r, y);
}

void fillBorderShadow(Context cr, Color color, in double x, in double y, in double w, in double h, in double d)
{
	static void fillOneBorder(Context cr, ColorD c, in int type, in double x, in double y, in double w, in double h)
	{
		auto pattern = type == 0 ? Pattern.createLinear(x, 0, x + w, 0) :
			type == 1 ? Pattern.createLinear(0, y, 0, y + h) :
			Pattern.createRadial(x, y, 0, x, y, w);
		scope(exit) pattern.destroy();

		pattern.addColorStopRgba( 0, c.tupleof); c.a /= 2;
		pattern.addColorStopRgba(.2, c.tupleof); c.a /= 2;
		pattern.addColorStopRgba(.4, c.tupleof); c.a /= 2;
		pattern.addColorStopRgba(.6, c.tupleof); c.a /= 2;
		pattern.addColorStopRgba(.8, c.tupleof); c.a = 0;
		pattern.addColorStopRgba( 1, c.tupleof);
		cr.setSource(pattern);
		cr.rectangle(x, y, w, h);
		cr.fill();
	}
	const cd = ColorD(color);
	fillOneBorder(cr, cd, 0, x + w, y, d, h);
	fillOneBorder(cr, cd, 0, x, y, -d, h);
	fillOneBorder(cr, cd, 1, x, y, w, -d);
	fillOneBorder(cr, cd, 1, x, y + h, w, d);
	fillOneBorder(cr, cd, 2, x, y, -d, -d);
	fillOneBorder(cr, cd, 2, x + w, y, d, -d);
	fillOneBorder(cr, cd, 2, x + w, y + h, d, d);
	fillOneBorder(cr, cd, 2, x, y + h, -d, d);
}

void paintRadial(Context cr, in Point p, in real d, in Color c)
{
	drawShadow(cr, p, 0, d, c);
}

void drawShadow(Context cr, in Point p, in real radius, in real d, in Color c)
{
	auto pattern = Pattern.createRadial(p.tupleof, radius, p.tupleof, radius + d);
	scope(exit) pattern.destroy();

	auto cd = ColorD(c);
	pattern.addColorStopRgba(0, cd.tupleof);
	cd.a = 0;
	pattern.addColorStopRgba(1, cd.tupleof);
	cr.setSource(pattern);

	if(!radius)
		cr.fillCircle(p, d);
	else
	{
		import std.math: PI;

		cr.moveTo(p.x + radius, p.y);
		cr.arc(p.tupleof, radius, 0, 2 * PI);
		cr.lineTo(p.x + radius + d, p.y);
		cr.arcNegative(p.tupleof, radius + d, 2 * PI, 0);
		cr.fill();
	}
}

void drawShadow(Context cr, in Segment seg, in real d, in Color color)
{
	const n = seg.vector.normal.withLength(d), p1 = seg.start - n, p2 = seg.start + n;

	auto pattern = Pattern.createLinear(p1.tupleof, p2.tupleof);
	scope(exit) pattern.destroy();

	auto cd = ColorD(color);
	pattern.addColorStopRgba( 0, cd.tupleof[0 .. 3], 0);
	pattern.addColorStopRgba(.5, cd.tupleof);
	pattern.addColorStopRgba( 1, cd.tupleof[0 .. 3], 0);
	cr.setSource(pattern);
	cr.moveTo(p1.tupleof);
	cr.lineTo(p2.tupleof);
	cr.lineTo((seg.end + n).tupleof);
	cr.lineTo((seg.end - n).tupleof);
	cr.fill();
}

void drawShadow(Context cr, in CircularArc arc, in real d, in Color color)
{
	const c = arc.circle.centre;
	const r1 = arc.circle.radius - d, r2 = arc.circle.radius + d;
	const n1 = arc.from * d, n2 = arc.to * d;
	const p1 = c + arc.from * r2, p2 = c + arc.to * r2;

	auto pattern = Pattern.createRadial(c.tupleof, r1, c.tupleof, r2);
	scope(exit) pattern.destroy();

	auto cd = ColorD(color);
	pattern.addColorStopRgba( 0, cd.tupleof[0 .. 3], 0);
	pattern.addColorStopRgba(.5, cd.tupleof);
	pattern.addColorStopRgba( 1, cd.tupleof[0 .. 3], 0);
	cr.setSource(pattern);

	Vector v1 = arc.from, v2 = arc.to;
	if(v1.crossproduct(v2) < 0)
	{
		import std.algorithm: swap;
		swap(v1, v2);
	}
	const a1 = v1.angleSignedValueXY, a2 = v2.angleSignedValueXY;
	cr.moveTo((c + v1 * r2).tupleof);
	cr.arc(c.tupleof, r2, a1, a2);
	cr.lineTo((c + v2 * r1).tupleof);
	cr.arcNegative(c.tupleof, r1, a2, a1);
	cr.fill();
}

void fillCircle(Context cr, in Point p, in real radius)
{
	import std.math: PI;

	cr.moveTo(p.x + radius, p.y);
	cr.arc(p.tupleof, radius, 0, 2 * PI);
	cr.fill();
}

void drawCircle(Context cr, in Point p, in real radius)
{
	import std.math: PI;

	cr.moveTo(p.x + radius, p.y);
	cr.arc(p.tupleof, radius, 0, 2 * PI);
	cr.stroke();
}

void moveToP(Context cr, in Point p)
{
	cr.moveTo(p.x, p.y);
}

void lineToP(Context cr, in Point p)
{
	cr.lineTo(p.x, p.y);
}
