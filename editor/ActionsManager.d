﻿/**
Editor project file.

Copyright: Denis Shelomovskij 2013

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module editor.ActionsManager;


import std.signals;

import editor.interfaces;


@safe:

abstract class Action
{
	void doIt()
	{
		run(true);
		_firstTime = false;
	}

	void undoIt()
	in { assert(!_firstTime); }
	body
	{
		run(false);
	}

protected:
	abstract void run(in bool forward);

private:
	bool _firstTime = true;
}


enum PerformedActionType
{
	forward = 1,
	doNew = 2 | forward,
	undo = 4,
	redo = 8 | forward,
	update = 0x10 | forward,
}

class ActionsManager
{
	// Constructor
	// ----------------------------------------------------------------------------------------------------

	this() inout pure nothrow
	{ actions = new inout(Action)[1024]; }

	// Action performed stuff
	// ----------------------------------------------------------------------------------------------------

	@trusted mixin Signal!(Action /*action*/, PerformedActionType /*type*/) actionPerformed;

	protected void onActionPerforming(Action action, in PerformedActionType type)
	{
		if(lastActionUpdating && type != PerformedActionType.update)
			stopLastActionUpdating();
	}

	protected void onActionPerformed(Action action, in PerformedActionType type)
	{
		actionPerformed.emit(action, type);
	}


	// Clear stuff
	// ----------------------------------------------------------------------------------------------------

	void clear()
	in { version(assert) assert(!performing); }
	out { assert(!currentSaved && !canUndo && !canRedo); }
	body
	{
		version(assert) { performing = true; scope(exit) performing = false; }

		immutable wasSaved = currentSaved;
		count = 0;
		currentIndex = -1;
		savedIndex = -2;
		marks = null;
		if(wasSaved)
			onCurrentSavedChanged();
		onCleared();
	}

	@trusted mixin Signal!() cleared;

	protected void onCleared() { cleared.emit(); }

	// Save stuff
	// ----------------------------------------------------------------------------------------------------

	@property bool currentSaved() @safe const pure nothrow
	{ return savedIndex == currentIndex; }

	void save()
	in { version(assert) assert(!performing); }
	out { assert(currentSaved); }
	body
	{ 
		version(assert) { performing = true; scope(exit) performing = false; }

		if(savedIndex == currentIndex)
			return;
		savedIndex = currentIndex;
		onCurrentSavedChanged();
	}

	@trusted mixin Signal!() currentSavedChanged;

	protected void onCurrentSavedChanged()
	{ currentSavedChanged.emit(); }

	// Marks stuff
	// ----------------------------------------------------------------------------------------------------

	@property bool isCurrentMarked(in uint id) @safe const pure nothrow
	{ return id in marks && marks[id] == currentIndex; }

	void setMark(in uint id) @safe pure nothrow
	in { version(assert) assert(!performing); }
	out { assert(isCurrentMarked(id)); }
	body
	{
		marks[id] = currentIndex;
	}

	// Functions
	// ----------------------------------------------------------------------------------------------------

	void addNew(Action newAction)
	in { version(assert) assert(!performing); }
	out { assert(!currentSaved && canUndo && !canRedo); }
	body
	{
		version(assert) { performing = true; scope(exit) performing = false; }

		immutable wasSaved = currentSaved;
		if(savedIndex > currentIndex)
			savedIndex = -2;
		() @trusted { // druntime @@@BUG5555@@@ workaround
		foreach(ref idx; marks.byValue()) if(idx > currentIndex)
			idx = -2;
		} ();

		onActionPerforming(newAction, PerformedActionType.doNew);
		++currentIndex;
		count = currentIndex + 1;
		if(actions.length < count)
			actions.length *= 2;
		actions[currentIndex] = newAction;

		newAction.doIt();
		if(wasSaved)
			onCurrentSavedChanged();
		onActionPerformed(newAction, PerformedActionType.doNew);
	}

	void undo()
	in { version(assert) assert(!performing && canUndo); }
	out { assert(canRedo); }
	body
	{
		version(assert) { performing = true; scope(exit) performing = false; }

		auto act = actions[currentIndex];
		onActionPerforming(act, PerformedActionType.undo);
		--currentIndex;
		act.undoIt();
		if(currentSaved || savedIndex == currentIndex + 1)
			onCurrentSavedChanged();
		onActionPerformed(act, PerformedActionType.undo);
	}

	void redo()
	in { version(assert) assert(!performing && canRedo); }
	out { assert(canUndo); }
	body
	{
		version(assert) { performing = true; scope(exit) performing = false; }

		auto act = actions[currentIndex + 1];
		onActionPerforming(act, PerformedActionType.redo);
		++currentIndex;
		act.doIt();
		if(currentSaved || savedIndex == currentIndex - 1)
			onCurrentSavedChanged();
		onActionPerformed(act, PerformedActionType.redo);
	}

	// Action updating stuff
	// ----------------------------------------------------------------------------------------------------

	@trusted mixin Signal!(Action /*updatingAction*/) lastActionUpdatingStarted;
	@trusted mixin Signal!(Action /*updatingAction*/) lastActionUpdatingStopped;
	@trusted mixin Signal!(Action /*updatedAction*/) lastActionUpdated;

	private bool lastActionUpdating = false;

	void startLastActionUpdating()
	in { assert(!lastActionUpdating && currentIndex != -1); }
	body
	{
		lastActionUpdating = true;
		lastActionUpdatingStarted.emit(actions[currentIndex]);
	}

	void stopLastActionUpdating()
	in { assert(lastActionUpdating); }
	body
	{
		lastActionUpdating = false;
		lastActionUpdatingStopped.emit(actions[currentIndex]);
	}

	void onLastActionUpdated()
	in { assert(lastActionUpdating); }
	body
	{
		auto act = actions[currentIndex];
		onActionPerforming(act, PerformedActionType.update);
		act.doIt();
		if(currentSaved)
		{
			savedIndex = -2;
			onCurrentSavedChanged();
		}
		() @trusted { // druntime @@@BUG5555@@@ workaround
		foreach(ref idx; marks.byValue()) if(idx == currentIndex)
			idx = -2;
		} ();
		onActionPerformed(act, PerformedActionType.update);
		lastActionUpdated.emit(act);
	}

	// Properties
	// ----------------------------------------------------------------------------------------------------

	@property @safe const pure nothrow
	{
		bool canUndo() 
		{ return currentIndex != -1; }

		bool canRedo()
		{ return count > currentIndex + 1; }
	}

private:
	version(assert) bool performing = false;
	Action[] actions;
	int count = 0, currentIndex = -1, savedIndex = -2;
	int[uint] marks = null;
}
