﻿/**
Editor project file.

Copyright: Denis Shelomovskij 2013

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module editor.FileManagerBase;


import std.signals;

import editor.interfaces;
import editor.ActionsManager;


@safe:

abstract class FileManagerBase: IEditorFileManager
{
	this(string unsavedTitle, string path)
	{
		this.unsavedTitle = unsavedTitle;

		if(!path)
		{
			createFile();
		}
		else
		{
			readFile(path);
			savedPath = path;
		}
		actionsManager.save();
	}

	// IEditorFileManager implementation
	// ----------------------------------------------------------------------------------------------------

	override @property inout(ActionsManager) actionsMgr() inout pure nothrow
	{ return actionsManager; }


	// IFileManager implementation
	// ----------------------------------------------------------------------------------------------------

	override bool trySave(out string error)
	{
		if(!tryWriteFile(savedPath, error))
			return false;
		actionsManager.save();
		return true;
	}

	override bool trySaveAs(string path, out string error)
	{
		if(!tryWriteFile(path, error))
			return false;
		savedPath = path;
		actionsManager.save();
		onTitleChanged();
		return true;
	}

	override bool trySaveCopyAs(string path, out string error)
	{
		return tryWriteFile(path, error);
	}

	override bool tryRevertToSaved(out string error)
	{
		if(!tryReadFile(savedPath, error))
			return false;
		actionsManager.clear();
		actionsManager.save();
		return true;
	}

	override void close()
	{ }


	override @property string path() const pure nothrow
	{ return savedPath; }


	override void undo()
	{ actionsManager.undo(); }

	override void redo()
	{ actionsManager.redo(); }

	// ----------------------------------------------------------------------------------------------------
	// Title stuff

	@property string title() const pure nothrow
	{ return savedPath ? savedPath : unsavedTitle; }

	@trusted mixin Signal!(string /*title*/) titleChanged;

	protected void onTitleChanged()
	{ titleChanged.emit(title); }

	@trusted mixin Signal!() dummy;

protected:
	string savedPath;
	ActionsManager actionsManager;

	abstract void createFile();
	abstract void readFile(string path);
	abstract bool tryReadFile(string path, out string error);
	abstract bool tryWriteFile(string path, out string error);

private:
	immutable string unsavedTitle;
}
