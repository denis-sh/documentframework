﻿/**
Editor project file.

Copyright: Denis Shelomovskij 2013

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module editor.hierarchy;


import std.algorithm, std.range;

import document.utils;


@safe:

interface IHierarchic
{
	@property inout(IHierarchic)[] parents() inout;
	@property inout(IHierarchic)[] children() inout;

	@property bool connected() const;
	protected @property void connected(bool val);

	final void connect()
	in { assert(!connected); }
	body
	{
		internalConnect();
		connected = true;
	}

	final void disconnect()
	in { assert(connected); }
	body
	{
		internalDisconnect();
		connected = false;
	}

	void internalConnect();
	void internalDisconnect();

	void connectParent(IHierarchic parent);
	void disconnectParent(IHierarchic parent);
}

bool containsAllAncestors(R)(R objects) @trusted /*Phobos @@@BUG@@@*/
if(isInputRange!R && is(ElementType!R : const IHierarchic))
{
	return objects.map!`a.children`().join()
		.all!(c => objects.canFind!`a is b`(c))();
}

IHierarchic[] withAllAncestors(R)(R objects) @trusted /*Phobos @@@BUG@@@*/
if(isInputRange!R && is(ElementType!R : IHierarchic))
out(res)
{
	assert(objects.map!`a.children`().join()
		.all!(c => res.canFind!`a is b`(c))());
}
body
{
	OrderedObjectSet!IHierarchic allObjects;

	void addWithChildren(IHierarchic obj)
	{
		if(allObjects.tryAppend(obj))
			foreach(child; obj.children)
				addWithChildren(child);
	}

	foreach(obj; objects)
		addWithChildren(obj);

	return allObjects.releaseBuff();
}

void connect(R)(R objects) @trusted /*Phobos @@@BUG@@@*/
if(isInputRange!R && is(ElementType!R : IHierarchic))
in
{
	assert(objects.all!`!a.connected`());
	assert(objects.map!`a.children`().join()
		.all!(c => objects.canFind!`a is b`(c))());
}
body
{
	foreach(obj; objects)
	{
		obj.connect();
		foreach(parent; obj.parents) if(!objects.canFind!`a is b`(parent))
			obj.connectParent(parent);
	}
}

void disconnect(R)(R objects) @trusted /*Phobos @@@BUG@@@*/
if(isInputRange!R && is(ElementType!R : IHierarchic))
in
{
	assert(objects.all!`a.connected`());
	assert(objects.map!`a.children`().join()
		.all!(c => objects.canFind!`a is b`(c))());
}
body
{
	foreach_reverse(obj; objects)
	{
		foreach(parent; obj.parents) if(!objects.canFind!`a is b`(parent))
				obj.disconnectParent(parent);
		obj.disconnect();
	}
}
