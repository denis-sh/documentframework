﻿/**
Editor project 2D file.

Copyright: Denis Shelomovskij 2013

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module editor.in2d.EditorBase;


import std.array;
import std.algorithm;
import std.range;
import std.signals;

import unstd.casts;

import geometry.in2d.point;

import editor.interfaces;
import editor.ActionsManager;
import editor.in2d.interfaces;
import editor.in2d.UIObject;

import document.utils;


@safe:

abstract class EditorBase
{
	// Constructor
	// ----------------------------------------------------------------------------------------------------

	this(ActionsManager actionsMgr, in real hoverDist)
	{
		_actionsMgr = actionsMgr;
		_hoverDist = hoverDist;
		actionsMgr.lastActionUpdatingStopped.connect(&onLastActionUpdatingStopped);
	}

	// Actions manager stuff
	// ----------------------------------------------------------------------------------------------------

	protected final @property inout(ActionsManager) actionsMgr() inout pure nothrow
	{ return _actionsMgr; }

	private ActionsManager _actionsMgr;

	// Draw stuff
	// ----------------------------------------------------------------------------------------------------

	@trusted mixin Signal!() objectsDrawing;
	@trusted mixin Signal!() objectsDrawn;

	protected void onObjectsDrawing(IObjectsDrawer drawer)
	{ objectsDrawing.emit(); }

	protected void onObjectsDrawn(IObjectsDrawer drawer)
	{ objectsDrawn.emit(); }

	final void draw(IObjectsDrawer drawer)
	{
		onObjectsDrawing(drawer);
		for(uint layer = 0; ; ++layer)
		{
			auto layerObjects = objectsBuff.filter!(o => o.visible && o.drawLayer == layer)();
			if(layerObjects.empty)
				break;
			foreach_reverse(d; cast(Dimensions) 0 .. Dimensions.max)
				foreach(obj; layerObjects.filter!(o => o.dimensions == d)())
					drawer.draw(obj);
		}
		if(areaSelectingState)
			drawer.drawAreaSelection(areaSelectionStartLocation, _mouse);
		onObjectsDrawn(drawer);
	}

	// Objects stuff
	// ----------------------------------------------------------------------------------------------------

	private OrderedObjectSet!UIObject _objects;

	final @property inout(UIObject)[] objectsBuff() inout pure nothrow
	{ return _objects.buff; }

	final @property inout(UIObject)[] objectsDup() inout pure nothrow
	{ return _objects.dup; }

	// Emitted when objects property is changed or objects are added or deleted.
	@trusted mixin Signal!() objectsChanged;

	@trusted mixin Signal!(string /*changedProperty*/, const UIObject[] /*changedObjects*/) objectsPropertyChanged;

	protected void onObjectsChanged()
	{ objectsChanged.emit(); }

	protected void onObjectsPropertyChanged(string changedProperty, UIObject[] changedObjects)
	{
		objectsPropertyChanged.emit(changedProperty, changedObjects);
		onObjectsChanged();
	}

	// Tool stuff
	// ----------------------------------------------------------------------------------------------------

	private ToolBase _tool = ToolBase.no;

	final @property ToolBase tool() const pure nothrow
	{ return _tool; }

	final void setTool(ToolBase tool)
	{
		if(_tool == tool)
			return;

		// TODO: Have to disable startX state here as we have
		// no information about new tool.
		canStartAreaSelection = canStartDrag = false;

		onToolChanging(tool);
		_tool = tool;

		unhoverIfNeeded();
		onToolChanged();
	}

	@trusted mixin Signal!(ToolBase /*newTool*/) toolChanging;
	@trusted mixin Signal!(ToolBase /*tool*/) toolChanged;

	protected void onToolChanging(ToolBase newTool)
	in { assert(!canStartDrag); }
	body
	{
		toolChanging.emit(newTool);
	}

	protected void onToolChanged()
	in { assert(!canStartDrag); }
	body
	{
		toolChanged.emit(_tool);
		updateHover();
	}

	// Selection stuff
	// ----------------------------------------------------------------------------------------------------

	private OrderedObjectSet!UIObject _selection;

	final @property inout(UIObject)[] selectionBuff() inout pure nothrow
	{ return _selection.buff; }

	final @property inout(UIObject)[] selectionDup() inout pure nothrow
	{ return _selection.dup; }

	@trusted mixin Signal!(const UIObject[] /*newSelection*/) selectionChanged;

	protected void onSelectionChanged()
	{
		selectionChanged.emit(selectionDup);
		if(!canStartDrag)
			updateHover();
	}

	final void selectAllVisible()
	{
		if(objectsBuff.all!`a.visible`())
			selectOnly(objectsBuff);
		else
			selectOnly(objectsBuff.filter!`a.visible`().array());
	}

	final protected
	{
		void selectNone()
		{ selectOnly(takeNone!(UIObject[])()); }

		void selectOnlyOne(UIObject obj)
		{ selectOnly(obj); }

		void selectOnly(UIObject[] objects...)
		{
			if(equal!`a is b`(selectionBuff, objects))
				return;

			foreach(selected; selectionBuff)
				selected.state._selected = false;

			_selection.copyFrom(objects);

			foreach(obj; selectionBuff)
				obj.state._selected = true;

			unhoverIfNeeded();
			onSelectionChanged();
		}

		void select(UIObject object)
		in { assert(!(object.state.selected)); }
		body
		{
			object.state._selected = true;
			_selection ~= object;
			unhoverIfNeeded();
			onSelectionChanged();
		}

		void deselect(UIObject object)
		in { assert(object.state.selected); }
		body
		{
			object.state._selected = false;
			_selection.remove(object);
			unhoverIfNeeded();
			onSelectionChanged();
		}
	}

	// Objects add/delete/set/create stuff
	// ----------------------------------------------------------------------------------------------------

	@trusted mixin Signal!(UIObject[] /*addingObjects*/) objectsAdding;
	@trusted mixin Signal!(UIObject[] /*addedObjects*/) objectsAdded;
	@trusted mixin Signal!(UIObject[] /*deletedObjects*/) objectsDeleted;

	protected void onObjectsAdding(UIObject[] addingObjects)
	{ objectsAdding.emit(addingObjects); }

	protected void onObjectsAdded(UIObject[] addedObjects)
	{
		objectsAdded.emit(addedObjects);
		onObjectsChanged();
		if(!canStartDrag)
			updateHover();
	}

	protected void onObjectsDeleted(UIObject[] deletedObjects)
	{
		objectsDeleted.emit(deletedObjects);
		onObjectsChanged();
	}

	final protected
	{
		void addObjects(UIObject[] objectsToAdd)
		in { assert(_objects.canAdd(objectsToAdd)); }
		body
		{
			if(objectsToAdd.empty)
				return;

			onObjectsAdding(objectsToAdd.dup);

			foreach(obj; objectsToAdd)
				_objects ~= obj;

			onObjectsAdded(objectsToAdd);
		}

		void deleteObjects(UIObject[] objectsToDelete)
		in { assert((() @trusted => all!(o => objectsBuff.canFind(o))(objectsToDelete)) ()); }
		body
		{
			if(objectsToDelete.empty)
				return;

			// Deselect first as it may change hover.
			auto selectionToDelete = objectsToDelete.filter!`a.state.selected`();
			if(!selectionToDelete.empty)
			{
				foreach(obj; selectionToDelete)
				{
					obj.state._selected = false;
					_selection.remove(obj);
				}
				unhoverIfNeeded();
				onSelectionChanged();
			}

			// Then unhover.
			immutable bool hoverDeleted = _hoverObject && objectsToDelete.canFind!`a is b`(_hoverObject);
			if(hoverDeleted)
				unhover();

			foreach(obj; objectsToDelete)
				_objects.remove(obj);

			onObjectsDeleted(objectsToDelete);

			if(hoverDeleted)
				updateHover();
		}

		private void deleteAllObjects()
		{
			if(objectsBuff.empty)
				return;

			if(_hoverObject)
				unhover();
			selectNone();

			auto prevObjects = _objects.releaseBuff();
			onObjectsDeleted(prevObjects);
		}

		void setObjects(UIObject[] objects)
		{
			deleteAllObjects();

			if(objects.empty)
				return;

			onObjectsAdding(objects.dup);
			_objects.copyFrom(objects);
			onObjectsAdded(_objects.dup);
		}
	}

	// Hover object stuff
	// ----------------------------------------------------------------------------------------------------

	private
	{
		UIObject _hoverObject;
		real _hoverDist;
	}

	final @property inout(UIObject) hoverObject() inout pure nothrow
	{ return _hoverObject; }

	final @property void hoverDist(in real val)
	{
		if(_hoverDist.tryUpdate(val) && haveMouse && !canStartDrag)
			updateHover();
	}

	@trusted mixin Signal!(const UIObject /*hoverObject*/) hoverChanged;
	@trusted mixin Signal!() hoverableChanged;

	protected void onHoverChanged()
	{ hoverChanged.emit(_hoverObject); }

	protected void onHoverableChanged()
	{ hoverableChanged.emit(); }

	protected abstract
	{
		// `hoverObject` may be invalid here as it can be called to check if it must be
		// unhovered because of tool or selection change.
		// Also tool/selection event handlers aren't called yet.
		bool canHover(in ToolBase tool, UIObject[] selectionBuff, UIObject obj) const;
	}

	final protected void forceHoverUpdate()
	{
		if(!canStartDrag)
			updateHover();
	}

	private
	{
		void unhover()
		in { assert(haveMouse && _hoverObject); }
		body
		{
			_hoverObject.state._hover = false;
			_hoverObject = null;
			canStartDrag = false;
			onHoverChanged();
		}

		void unhoverIfNeeded()
		{
			if(_hoverObject && !(_hoverObject.visible && canHover(_tool, selectionBuff, _hoverObject)))
				unhover();
		}

		void updateHover()
		in { assert(!canStartDrag); }
		body
		{
			immutable hoverDistSq = _hoverDist ^^ 2;
			real newDistSq = real.nan;
			UIObject newHoverObject = null;
			bool wasAllHoverable = true, hoverableChanged = false, canHoverAny = false;

			foreach(obj; objectsBuff)
			{
				if(!obj.visible)
				{
					obj.state._unhoverable = true;
					continue;
				}

				wasAllHoverable &= !obj.state._unhoverable;
				hoverableChanged |= obj.state._unhoverable.tryUpdate(!canHover(_tool, selectionBuff, obj));
				if(obj.state._unhoverable)
					continue;
				canHoverAny = true;

				if(!haveMouse)
					continue;

				immutable real currDistSq = obj.mouseDistanceSq(_mouse);

				if(currDistSq > hoverDistSq)
					continue;

				// Select with smallest dimensions if can.
				// For equal dimensions select nearest.
				immutable ddim = obj.dimensions - (newHoverObject ? newHoverObject.dimensions : Dimensions.max);
				if(ddim < 0 || !ddim && currDistSq < newDistSq)
				{
					newDistSq = currDistSq;
					newHoverObject = obj;
				}
			}

			if(!canHoverAny)
			{
				foreach(obj; objectsBuff)
					obj.state._unhoverable = false;
				if(wasAllHoverable)
					hoverableChanged = false;
			}

			if(newHoverObject !is _hoverObject)
			{
				if(_hoverObject)
					_hoverObject.state._hover = false;
				_hoverObject = newHoverObject;
				if(_hoverObject)
					_hoverObject.state._hover = true;

				onHoverChanged();
			}

			if(hoverableChanged)
				onHoverableChanged();
		}
	}

	// Mouse event stuff
	// ----------------------------------------------------------------------------------------------------

	private
	{
		bool haveMouse = false;
		Point _mouse;

		bool canStartDrag;
		bool canStartAreaSelection;
	}

	protected abstract
	{
		void onMouseDownOnHover(in ToolBase tool, UIObject[] selection, UIObject hoverObject, in Point mouse, out bool canStartDrag);
		void onMouseDownNoHover(in ToolBase tool, UIObject[] selection, in Point mouse, out bool canStartAreaSelection);
		void onMouseDoubleDownOnHover(in ToolBase tool, UIObject[] selection, UIObject hoverObject);
		void onMouseDoubleDownNoHover(in ToolBase tool, UIObject[] selection, in Point mouse);
	}

	void onMouseDown()
	in { assert(haveMouse); }
	body
	{
		if(_hoverObject)
		{
			onMouseDownOnHover(_tool, selectionDup, _hoverObject, _mouse, canStartDrag);
			if(canStartDrag)
				dragStartLocation = _mouse;
		}
		else
		{
			onMouseDownNoHover(_tool, selectionDup, _mouse, canStartAreaSelection);
			if(canStartAreaSelection)
				areaSelectionStartLocation = _mouse;
		}
	}

	void onMouseDoubleDown()
	in { assert(haveMouse); }
	body
	{
		if(_hoverObject)
			onMouseDoubleDownOnHover(_tool, selectionDup, _hoverObject);
		else
			onMouseDoubleDownNoHover(_tool, selectionDup, _mouse);
	}

	void onMouseUp()
	in { assert(haveMouse); }
	body
	{
		canStartAreaSelection = canStartDrag = false;
		if(_draggingState)
		{
			stopDragging();
		}
		else if(areaSelectingState)
		{
			stopAreaSelecting();
		}
	}

	void onMouseMove(in Point mouse)
	{
		if(haveMouse && mouse == _mouse)
			return;

		_mouse = mouse;
		haveMouse = true;

		if(canStartDrag)
		{
			startDragging();
			canStartDrag = false;
		}
		else if(_draggingState)
		{
			updateDragging();
		}
		else if(canStartAreaSelection)
		{
			startAreaSelecting();
			canStartAreaSelection = false;
		}
		else if(areaSelectingState)
		{
			updateAreaSelecting();
		}
		else
		{
			updateHover();
		}
	}

	// Dragging stuff
	// ----------------------------------------------------------------------------------------------------

	private
	{
		int _draggingState;
		Point dragStartLocation;
		Action dragAction;
	}

	final @property int draggingState() const pure nothrow
	{ return _draggingState; }

	@trusted mixin Signal!() dragged;
	@trusted mixin Signal!() dragStopped;

	protected void onDragged()
	{ dragged.emit(); }

	protected void onDragStopped()
	{ dragStopped.emit(); }

	protected abstract
	{
		int dragStart(UIObject[] selection, UIObject hoverObject, in Point dragStartLocation, in Point mouse, out UIObject[] objectsToDrag, out Action dragAction);
		void dragTo(in int draggingState, in Point mouse);
	}

	private
	{
		void startDragging()
		in { assert(!_draggingState && _hoverObject); }
		out { assert(_draggingState); }
		body
		{
			UIObject[] objectsToDrag;
			immutable int newDraggingState = dragStart(selectionDup, _hoverObject, dragStartLocation, _mouse, objectsToDrag, dragAction);
			foreach(obj; objectsToDrag)
				obj.state._dragging = true;
			actionsMgr.addNew(dragAction);
			actionsMgr.startLastActionUpdating();
			_draggingState = newDraggingState;
		}

		void updateDragging()
		in { assert(_draggingState); }
		body
		{
			dragTo(_draggingState, _mouse);
			onDragged();
		}

		void stopDragging(in bool stopLastActionUpdating = true)
		in { assert(_draggingState); }
		out { assert(!_draggingState); }
		body
		{
			if(stopLastActionUpdating)
				actionsMgr.stopLastActionUpdating();
			dragAction = null;
			_draggingState = 0;

			foreach(obj; selectionBuff)
				obj.state._dragging = false;

			onDragStopped();
			updateHover();
		}

		void onLastActionUpdatingStopped(Action action)
		{
			if(action is dragAction)
				stopDragging(false);
		}
	}

	// Area selection stuff
	// ----------------------------------------------------------------------------------------------------

	private
	{
		int areaSelectingState;
		Point areaSelectionStartLocation;
	}

	@trusted mixin Signal!() areaSelectionChanged;
	@trusted mixin Signal!() areaSelectingStopped;

	protected void onAreaSelectionChanged()
	{ areaSelectionChanged.emit(); }

	protected void onAreaSelectingStopped()
	{ areaSelectingStopped.emit(); }

	protected abstract
	{
		int areaSelectingStart(in ToolBase tool);
		bool inAreaSelection(in int areaSelectingState, UIObject obj, in Point from, in Point to);
	}

	private
	{
		void startAreaSelecting()
		in { assert(!areaSelectingState); }
		out { assert(areaSelectingState); }
		body
		{
			areaSelectingState = areaSelectingStart(_tool);
		}

		void updateAreaSelecting()
		in { assert(areaSelectingState); }
		body
		{
			UIObject[] newSelection = objectsBuff
				.filter!(o => o.visible && inAreaSelection(areaSelectingState, o, areaSelectionStartLocation, _mouse))().array();
			selectOnly(newSelection);
			onAreaSelectionChanged();
		}

		void stopAreaSelecting()
		in { assert(areaSelectingState); }
		out { assert(!areaSelectingState); }
		body
		{
			areaSelectingState = 0;
			onAreaSelectingStopped();
		}
	}
}
