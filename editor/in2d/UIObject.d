﻿/**
Editor project 2D file.

Copyright: Denis Shelomovskij 2013

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module editor.in2d.UIObject;


import geometry.in2d.point;
import geometry.in2d.box;


@safe:

// UI objects
// ----------------------------------------------------------------------------------------------------

enum Dimensions
{
	point = 0,
	curve = 1,
	surface = 2,
	max = 3,
}

// UI object abstract class
// ----------------------------------------------------------------------------------------------------

abstract class UIObject
{
	// Dimensions stuff
	// ----------------------------------------------------------------------------------------------------

	immutable Dimensions dimensions;
	immutable uint drawLayer;

	final @property const pure nothrow
	{
		bool isPoint() { return dimensions == Dimensions.point; }
		bool isCurve() { return dimensions == Dimensions.curve; }
		bool isSurface() { return dimensions == Dimensions.surface; }
	}

	// State stuff
	// ----------------------------------------------------------------------------------------------------

	struct State
	{
		@property const pure nothrow
		{
			bool hover()  { return _hover; }
			bool selected()  { return _selected; }
			bool dragging()  { return _dragging; }
			bool unhoverable()  { return _unhoverable; }

			bool normal()  { return !hover && !selected && !dragging && !unhoverable ; }
		}

		package bool _hover = false, _selected = false, _dragging = false, _unhoverable = false;
	}

	enum State normalState = State.init;
	static assert(normalState.normal);

	State state;

	// Constructor
	// ----------------------------------------------------------------------------------------------------

	this(in Dimensions dimensions, in uint drawLayer = 0) inout pure nothrow
	{ this.dimensions = dimensions; this.drawLayer = drawLayer; }

	// Abstract functions and properties
	// ----------------------------------------------------------------------------------------------------

	abstract
	{
		@property bool visible() const;
		@property Box box() const;
		real mouseDistanceSq(in Point mouse) const;
	}
}
