﻿/**
Editor project 2D file.

Copyright: Denis Shelomovskij 2013

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module editor.in2d.ViewManager;


import std.algorithm;
import std.signals;

import geometry.in2d.size;
import geometry.in2d.point;
import geometry.in2d.vector;


@safe:

class ViewManager
{
	// Constructor
	// ----------------------------------------------------------------------------------------------------

	this(in real windowPadding, in SizeR visibleAreaSize, in Point visibleAreaCentre = Point.origin) inout pure nothrow
	{
		_visibleAreaSize = visibleAreaSize;
		_visibleAreaCentre = visibleAreaCentre;
		_windowPadding = windowPadding;
	}

	// Window properties stuff
	// ----------------------------------------------------------------------------------------------------

	@property void windowSize(in SizeI sz)
	{
		if(_haveWindowSize && windowSize == sz)
			return;
		_haveWindowSize = true;
		_windowSize = sz;
		updateVisibleToWindowScale();
	}

	@property SizeI windowSize() const pure nothrow
	{ return _windowSize; }

	@property void windowPadding(in real padding)
	{
		if(_windowPadding == padding)
			return;
		_windowPadding = padding;
		if(_haveWindowSize)
			updateVisibleToWindowScale();
	}

	private
	{
		SizeI _windowSize;
		bool _haveWindowSize = false;
		real _windowPadding;
	}

	// Window mouse stuff
	// ----------------------------------------------------------------------------------------------------

	@property void windowMouse(in Point m)
	in { assert(!(!_haveWorldMouse && dragging)); }
	body
	{
		if(_haveWindowMouse && _windowMouse == m)
			return;
		_haveWindowMouse = true;
		_windowMouse = m;
		if(dragging)
			drag();
		else
			tryUpdateWorldMouse();
	}

	private
	{
		Point _windowMouse;
		bool _haveWindowMouse = false;
	}

	// World mouse stuff
	// ----------------------------------------------------------------------------------------------------

	@trusted mixin Signal!(Point /*worldMouse*/) worldMouseMoved;

	private
	{
		Point _worldMouse;
		bool _haveWorldMouse = false;

		void tryUpdateWorldMouse()
		{
			if(!_haveWindowMouse || !_haveWindowSize)
				return;
			immutable m = windowPointToWorld(_windowMouse);
			if(_haveWorldMouse && _worldMouse == m)
				return;
			_haveWorldMouse = true;
			_worldMouse = m;
			worldMouseMoved.emit(_worldMouse);
		}
	}

	// Visible area stuff
	// ----------------------------------------------------------------------------------------------------

	@property void visibleAreaSize(in SizeR sz)
	{
		if(_visibleAreaSize == sz)
			return;
		_visibleAreaSize = sz;
		updateVisibleToWindowScale();
	}

	@property SizeR visibleAreaSize() const pure nothrow
	{ return _visibleAreaSize; }

	@property void visibleAreaCentre(in Point c)
	{
		if(_visibleAreaCentre == c)
			return;
		_visibleAreaCentre = c;
		tryUpdateWorldMouse();
	}

	@property Point visibleAreaCentre() const pure nothrow
	{ return _visibleAreaCentre; }

	private
	{
		SizeR _visibleAreaSize;
		Point _visibleAreaCentre = Point.origin;
	}


	// Dragging stuff
	// ----------------------------------------------------------------------------------------------------

	@trusted mixin Signal!(Vector /*delta*/) dragged;

	@property bool dragging() const pure nothrow
	{ return _dragging; }

	void tryStartDragging()
	{ _dragging = true; }

	void tryStopDragging()
	{ _dragging = false; }

	void drag()
	in { assert(_dragging); }
	body
	{
		immutable v = _worldMouse - windowPointToWorld(_windowMouse);
		_visibleAreaCentre += v;
		dragged.emit(v);
	}

	private
	{
		bool _dragging = false;
	}


	// Scale stuff
	// ----------------------------------------------------------------------------------------------------

	@trusted mixin Signal!(real /*zoomScale*/) zoomScaleChanged;
	@trusted mixin Signal!(real /*scale*/) scaleChanged;

	@property real zoomScale() const pure nothrow
	{ return _zoomScale; }

	@property real scale() const pure nothrow
	{ return _scale; }

	void zoom(in real zoomScale)
	{ zoom(_worldMouse, zoomScale); }

	void zoom(in Point centre, in real zoomScale)
	in { assert(_haveWindowSize); }
	body
	{
		immutable sc = _zoomScale * zoomScale;
		if(sc > maxZoomScale || sc < minZoomScale)
			return;
		_zoomScale = sc;
		_visibleAreaCentre = ((_visibleAreaCentre.asVector + centre.asVector * (zoomScale - 1)) / zoomScale).asPoint;
		updateScale();
		zoomScaleChanged.emit(_zoomScale);
	}

	private
	{
		enum maxZoomScale = 50; // only 50 instead of 1000 because of Cairo gradient fill
		enum minZoomScale = .001;
		real _zoomScale = 1;

		real _visibleToWindowScale = real.nan;

		// <window size> = <world size> * scale
		real _scale = real.nan;


		void updateVisibleToWindowScale()
		in { assert(_haveWindowSize); }
		body
		{
			auto adjustedWindowSize = SizeR(_windowSize.tupleof);
			immutable minSize = _windowPadding * 10;
			void processSize(ref real size) const
			{
				if(size > minSize + _windowPadding * 2)
					size -= _windowPadding * 2;
				else if(size > minSize)
					size -= size - minSize;
			}
			processSize(adjustedWindowSize.width);
			processSize(adjustedWindowSize.height);
			immutable k = min(adjustedWindowSize.width / _visibleAreaSize.width, adjustedWindowSize.height / _visibleAreaSize.height);
			if(_visibleToWindowScale == k)
				return;
			_visibleToWindowScale = k;
			updateScale();
		}

		void updateScale()
		in { assert(_haveWindowSize); }
		body
		{
			immutable k = _zoomScale * _visibleToWindowScale;
			if(_scale == k)
				return;
			_scale = k;
			scaleChanged.emit(_scale);
			tryUpdateWorldMouse();
		}
	}

	// Resetting functions
	// ----------------------------------------------------------------------------------------------------

	void resetZoomScale()
	{
		if(_zoomScale == 1)
			return;
		_dragging = false;
		_zoomScale = 1;
		updateScale();
	}

	void resetView()
	{
		_dragging = false;
		if(_visibleAreaCentre != Point.origin)
		{
			immutable v = -_visibleAreaCentre.asVector;
			_visibleAreaCentre = Point.origin;
			dragged.emit(v);
		}
		if(_zoomScale != 1)
			resetZoomScale();
		else
			tryUpdateWorldMouse();
	}

private:

	// Private functions
	// ----------------------------------------------------------------------------------------------------

	Point windowPointToWorld(in Point windowPoint) const pure nothrow
	in { assert(_haveWindowSize); }
	body
	{
		const v = (windowPoint - Point((_windowSize / 2.0).tupleof)) / _scale;
		return _visibleAreaCentre + v;
	}
}
