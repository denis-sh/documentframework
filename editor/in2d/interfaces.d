﻿/**
Editor project 2D file.

Copyright: Denis Shelomovskij 2013

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module editor.in2d.interfaces;


import geometry.in2d.point;

import editor.in2d.UIObject;
import editor.in2d.ViewManager;


@safe:

interface IObjectsDrawer
{
	void begin(ViewManager viewManager);
	void end();
	void draw(in Object object);
	void drawAreaSelection(in Point from, in Point to);
}
