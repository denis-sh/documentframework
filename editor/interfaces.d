﻿/**
Editor project file.

Copyright: Denis Shelomovskij 2013

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module editor.interfaces;


public import document.interfaces;

import editor.ActionsManager;


@safe:

interface IEditorFileManager: IFileManager, IKeyListener, IDrawable
{
	@property inout(ActionsManager) actionsMgr() inout pure nothrow;
}

enum ToolBase : long
{ no }
